//#############################################################################
//# Purpose: etrace registers                                                 #
//#############################################################################

`include "etrace_regmap.vh"
module etrace_regs (/*AUTOARG*/
   // Outputs
   s_access_out, s_packet_out, cfg_trace_enable, cfg_loop_enable,
   cfg_async_mode, cfg_samplerate, dma_en, chainmode, datamode,
   ctrlmode, stride_reg, dma_count_reg, dstaddr_reg, srcaddr_reg,
   mem_access,
   // Inputs
   clk, nreset, s_access_in, s_packet_in, dma_state, update,
   dma_count, dstaddr, srcaddr, trace_fifo_full, trace_fifo_empty,
   datasample_fifo
   );
   // parameters
   parameter  ID      = 999;
   parameter  AW      = 32;        // address width
   parameter  PDW     = 32;        // packet data width
   parameter  PW      = 2*AW+40;   // emesh packet width
   parameter  DEF_CFG = 0;         // default config after reset

   parameter VW     = 32;           // width of vector to sample
   parameter DW     = 32;        // width of counter
   parameter MW     = VW+DW;        // memory width should be 64,128,256
   parameter CW     = 15;           // config register width

   localparam RFAW    = 6;         // register block size

   //################################
   //# Register Access
   //################################
   //Clock and reset
   input            clk;
   input            nreset;

   // packet in
   input 	       s_access_in;
   input [PW-1:0]  s_packet_in;

   // packet out
   output          s_access_out;
   output [PW-1:0] s_packet_out;

   // etrace config
   output          cfg_trace_enable;
   output          cfg_loop_enable;
   output          cfg_async_mode;
   output [3:0]    cfg_samplerate;

   // dma config
   output          dma_en;
   output 	       chainmode;     // idle when done
   input [3:0]     dma_state;
   input           update;

   output [1:0]    datamode;
   output [4:0]    ctrlmode;
   output [31:0]   stride_reg;

   input [31:0]    dma_count;
   output [31:0]   dma_count_reg;

   input [31:0]    dstaddr;
   output [31:0]   dstaddr_reg;

   input [31:0]    srcaddr;
   output [31:0]   srcaddr_reg;

   // trace fifo full
   input          trace_fifo_full;
   input          trace_fifo_empty;

   //################################
   //# Trace buffer Access
   //################################
   input [MW-1:0] datasample_fifo;
   output         mem_access;

   // pipeline regs
   reg            cfg_sel;
   reg            trace_sel;
   reg            trace_hi_sel;

   reg            s_access_out;
   reg [1:0]      datamode_out;
   reg [4:0]      ctrlmode_out;
   reg [AW-1:0]   dstaddr_out;

   // trace read cache
   reg [MW-1:0]   datasample_cache;

   // local regs
   reg [CW-1:0]   cfg_reg;
   reg [PDW-1:0]  reg_data_out;

   // local dma regs
   reg [31:0] 	  dma_count_reg;
   reg [31:0] 	  stride_reg;
   reg [AW-1:0]   dstaddr_reg;
   reg [AW-1:0]   srcaddr_reg;
   reg [31:0] 	  status_reg;

   // packet
   wire           s_access_in;
   wire [PW-1:0]  s_packet_in;

   wire [AW-1:0]  srcaddr_out;

   wire           s_wait_out;
   wire           s_wait_in;

   //
   wire [MW-1:0]   datasample_combined;
   wire            mem_read_access;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [4:0]           ctrlmode_in;            // From p2e0 of packet2emesh.v
   wire [AW-1:0]        data_in;                // From p2e0 of packet2emesh.v
   wire [PDW-1:0]       data_out;               // From mux0 of oh_mux3.v
   wire [1:0]           datamode_in;            // From p2e0 of packet2emesh.v
   wire [AW-1:0]        dstaddr_in;             // From p2e0 of packet2emesh.v
   wire [AW-1:0]        srcaddr_in;             // From p2e0 of packet2emesh.v
   wire                 write_in;               // From p2e0 of packet2emesh.v
   // End of automatics

   //###########################
   //# PACKET PARSING
   //###########################
   packet2emesh #(.AW(AW),
                  .PW(PW))
   p2e0 (.packet_in                     (s_packet_in[PW-1:0]),
         /*AUTOINST*/
         // Outputs
         .write_in                      (write_in),
         .datamode_in                   (datamode_in[1:0]),
         .ctrlmode_in                   (ctrlmode_in[4:0]),
         .dstaddr_in                    (dstaddr_in[AW-1:0]),
         .srcaddr_in                    (srcaddr_in[AW-1:0]),
         .data_in                       (data_in[AW-1:0]));

   // Read write decode
   assign reg_access = s_access_in &
                (dstaddr_in[31:20]   ==ID) &
                (dstaddr_in[19:16]   ==`ETRACE_REGS);
   assign mem_read_access = s_access_in &
                (dstaddr_in[31:20]   ==ID) &
                (dstaddr_in[19:16]   ==`ETRACE_MEM);
   assign mem_cache_access = s_access_in &
                   (dstaddr_in[31:20] ==ID) &
                   (dstaddr_in[19:16] ==`ETRACE_MEM) &
                   (dstaddr_in[2:0] != 3'b000);        // refill cache
   assign mem_access = mem_read_access & ~mem_cache_access;

   // Read operation
   assign reg_read = reg_access & ~write_in;

   // Write to the register file
   assign reg_write = reg_access & write_in;

   // Config write enables
   assign config_write = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_CFG);
   assign stride_write     = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_STRIDE);
   assign count_write      = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_COUNT);
   assign srcaddr0_write    = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_SRCADDR);
   //assign srcaddr1_write   = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_SRCADDR64);
   assign dstaddr0_write   = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_DSTADDR);
   //assign dstaddr1_write   = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_DSTADDR64);
   assign status_write     = reg_write & (dstaddr_in[RFAW+1:2]==`ETRACE_STATUS);

   //###########################
   //# CONFIG
   //###########################
   always @ (posedge clk)
     if(!nreset)
       cfg_reg[CW-1:0] <= DEF_CFG;
     else if (config_write)
       // TODO check this logic:- use datamode_in[1:0]?
       cfg_reg[CW-1:0] <= data_in[CW-1:0];

   assign cfg_trace_enable  = cfg_reg[0];
   assign cfg_loop_enable   = cfg_reg[1];//runs forever as circular buffer
   assign cfg_async_mode    = cfg_reg[2];//treats input signals as async/sync
   assign cfg_samplerate    = cfg_reg[7:4];
   /*
    * 100MS/s
    * 50MS/s
    * 25MS/s
    * 12.5MS/s
    * 6.25MS/s
    * 3.125Ms/s
    * 1.0Ms/s
    * 0.5Ms/s
    */

   assign dma_en             = cfg_reg[8];     // dma enabled
   assign chainmode          = cfg_reg[10];    // idle when done
   assign irqmode            = cfg_reg[12];    // enable irq at end of transfer
   // only caching for s_packet_out - TODO extend for master access?
   generate
      if (DW == 32)
        assign datamode[1:0] = 2'b11; // cfg_reg[14:13]; // datamode (8/16/32/64 bits)
      else // DW == 16
        assign datamode[1:0] = 2'b10;
   endgenerate
   assign ctrlmode[4:0]      = 5'b0;           // bits 10-11 reserved

   //################################
   //# STRIDE
   //################################
   always @ (posedge clk)
     if(stride_write)
       stride_reg[31:0]  <= data_in[31:0];

   //################################
   //# COUNT
   //################################
   always @ (posedge clk)
     if(count_write)
       dma_count_reg[31:0] <= data_in[31:0];
     else if (update)
       dma_count_reg[31:0] <= dma_count[31:0];

   //################################
   //# SRCADDR
   //################################
   always @ (posedge clk)
     if(srcaddr0_write)
       srcaddr_reg[31:0]  <= data_in[31:0];
     //else if(srcaddr1_write)
     //  srcaddr_reg[63:32] <= data_in[31:0];
     else if (update)
       srcaddr_reg[AW-1:0] <= srcaddr[AW-1:0];

   //################################
   //# DSTADDR
   //################################
   always @ (posedge clk)
     if(dstaddr0_write)
       dstaddr_reg[31:0]  <= data_in[31:0];
     //else if(dstaddr1_write)
     //  dstaddr_reg[63:32] <= data_in[31:0];
     else if (update)
       dstaddr_reg[AW-1:0] <= dstaddr[AW-1:0];

   //################################
   //# STATUS
   //################################
   always @ (posedge clk)
     if(status_write)
       status_reg[31:0]  <= data_in[31:0];
     else if (config_write) // next descriptor
       status_reg[31:0]  <= {{(32-4){1'b0}},
			                 dma_state[3:0]};

   //###############################
   //# DATA READBACK MUX
   //###############################
   always @ (posedge clk)
     if(reg_read)
       case(dstaddr_in[RFAW+1:2])
         `ETRACE_CFG:       reg_data_out[PDW-1:0] <= {trace_fifo_full, trace_fifo_empty, {(32-CW-2){1'b0}}, cfg_reg[CW-1:0]};
         `ETRACE_STRIDE:    reg_data_out[PDW-1:0] <= stride_reg[31:0];
         `ETRACE_COUNT:     reg_data_out[PDW-1:0] <= dma_count_reg[31:0];
         `ETRACE_SRCADDR:   reg_data_out[PDW-1:0] <= srcaddr_reg[31:0];
         //`ETRACE_SRCADDR64: reg_data_out[PDW-1:0] <= srcaddr_reg[63:32];
         `ETRACE_DSTADDR:   reg_data_out[PDW-1:0] <= dstaddr_reg[31:0];
         //`ETRACE_DSTADDR64: reg_data_out[PDW-1:0] <= dstaddr_reg[63:32];
         `ETRACE_STATUS:    reg_data_out[PDW-1:0] <= status_reg[31:0];
         default: reg_data_out[PDW-1:0] <= 32'd0;
       endcase // case (dstaddr_in[RFAW+1:2])
     else
       reg_data_out[PDW-1:0] <= 32'd0;

   //###############################
   //# FORWARD PACKET TO OUTPUT
   //###############################

   // always accept new data from axi-s interface
   // TODO check this logic
   assign s_wait_in = 1'b0;

   // assume rr will never be blocked
   assign s_wait_out = 1'b0;

   // pipeline
   always @ (posedge clk)
     begin
        // if(~s_wait_out)
        s_access_out <= s_access_in & ~write_in;

        cfg_sel <= reg_access;
        trace_sel <= mem_access;
        trace_hi_sel <= mem_cache_access;
        // TODO extend for byte access
     end

   //packet
   always @ (posedge clk)
     if(~s_wait_out & s_access_in & ~write_in)
       begin
      datamode_out[1:0]   <= datamode_in[1:0];
      ctrlmode_out[4:0]   <= ctrlmode_in[4:0];
      dstaddr_out[AW-1:0] <= srcaddr_in[AW-1:0];
       end

   generate
      if (DW == 32)
        begin: trace_32
          assign datasample_combined[31:0] = datasample_fifo[DW-1:0];
          assign datasample_combined[63:32] = datasample_fifo[MW-1:DW];
        end
      else // DW == 16
        begin
          assign datasample_combined[31:0] = {datasample_fifo[MW-1:DW], datasample_fifo[DW-1:0]};
        end // always @ (posedge clk)
   endgenerate

   // cache the high data for 32 bit access
   always @ (posedge clk)
     if (trace_sel)
       datasample_cache[MW-1:0] <= datasample_fifo[MW-1:0];

   oh_mux3 #(.DW(PDW))
   mux0 (// Outputs
         .out (data_out[PDW-1:0]),
         // Inputs
         // TODO check this logic:- use datamode_in[1:0]?
         .in0 (reg_data_out[PDW-1:0]), .sel0 (cfg_sel),
         .in1 (datasample_combined[PDW-1:0]), .sel1 (trace_sel),
         .in2 ({{(PDW-DW){1'b0}}, datasample_cache[MW-1:DW]}), .sel2 (trace_hi_sel)
         /*AUTOINST*/);

   oh_mux2 #(.DW(AW))
   mux1 (// Outputs
         .out (srcaddr_out[AW-1:0]),
         // Inputs
         .in0 ({(AW){1'd0}}), .sel0 (~trace_sel),
         .in1 ({{(AW-DW){1'b0}}, datasample_combined[MW-1:DW]}), .sel1 (trace_sel)
         /*AUTOINST*/);

   emesh2packet #(.AW(AW))
   e2p (.packet_out			(s_packet_out[PW-1:0]),
        .write_out                      (1'b1),
	    /*AUTOINST*/
        // Inputs
        .datamode_out                   (datamode_out[1:0]),
        .ctrlmode_out                   (ctrlmode_out[4:0]),
        .dstaddr_out                    (dstaddr_out[AW-1:0]),
        .data_out                       (data_out[AW-1:0]),
        .srcaddr_out                    (srcaddr_out[AW-1:0]));

endmodule // etrace_regs

// Local Variables:
// verilog-library-directories:("." "../../emesh/hdl" "../../stdlib/rtl")
// End:
