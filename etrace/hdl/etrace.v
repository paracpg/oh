//#############################################################################
//# Author:   Peter Saunderson                                                #
//# License:  MIT (see LICENSE file in OH! repository)                        #
//#############################################################################
module etrace (/*AUTOARG*/
   // Outputs
   data_txwr_access, data_txwr_packet, s_access_out, s_packet_out,
   // Inputs
   trace_clk, trace_trigger, trace_vector, clk, nreset,
   data_txwr_wait, s_access_in, s_packet_in
   );

   parameter TARGET = "GENERIC";    // XILINX,ALTERA,GENERIC
   parameter VW     = 16;           // width of vector to sample
   parameter DW     = 16;           // width of counter
   parameter AW     = 32;           // width of address input bus
   parameter PDW    = AW;           // packet data width
   parameter DEPTH  = 1024;         // memory depth
   parameter ID     = 999;
   parameter NAME   = "my";

   parameter PW     = 104;          // packet width
   localparam MW    = VW+DW;        // memory width should be 64,128,256

   //Logic analyzer interface
   input            trace_clk;
   input            trace_trigger;
   input [VW-1:0]   trace_vector;

   //Clock and reset
   input            clk;
   input            nreset;

   //Data streaming interface
   input            data_txwr_wait;
   output           data_txwr_access;
   output [PW-1:0]  data_txwr_packet;

   //Config interface
   input            s_access_in;
   input [PW-1:0]   s_packet_in;
   output           s_access_out;
   output [PW-1:0]  s_packet_out;

   //local regs
   reg [DW-1:0]     cycle_counter;
   reg [VW-1:0]     trace_vector_reg;
   reg [VW-1:0]     trace_vector_shadow_reg;
   reg              trace_fifo_full;
   reg              trace_fifo_empty;

   /*AUTOREG*/

   wire                 change_detect;
   wire                 change_detect_shadow;
   wire                 fifo_read_wait;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 access_out;             // From etrace_fifo of etrace_fifo.v
   wire                 cfg_async_mode;         // From etrace_regs of etrace_regs.v
   wire                 cfg_loop_enable;        // From etrace_regs of etrace_regs.v
   wire [3:0]           cfg_samplerate;         // From etrace_regs of etrace_regs.v
   wire                 cfg_trace_enable;       // From etrace_regs of etrace_regs.v
   wire                 chainmode;              // From etrace_regs of etrace_regs.v
   wire [4:0]           ctrlmode;               // From etrace_regs of etrace_regs.v
   wire [1:0]           datamode;               // From etrace_regs of etrace_regs.v
   wire [MW-1:0]        datasample_fifo;        // From etrace_fifo of etrace_fifo.v
   wire [31:0]          dma_count;              // From etrace_fifo of etrace_fifo.v
   wire [31:0]          dma_count_reg;          // From etrace_regs of etrace_regs.v
   wire                 dma_en;                 // From etrace_regs of etrace_regs.v
   wire                 dma_master_active;      // From etrace_ctrl of etrace_ctrl.v
   wire [3:0]           dma_state;              // From etrace_ctrl of etrace_ctrl.v
   wire [AW-1:0]        dstaddr;                // From etrace_fifo of etrace_fifo.v
   wire [31:0]          dstaddr_reg;            // From etrace_regs of etrace_regs.v
   wire                 empty;                  // From etrace_fifo of etrace_fifo.v
   wire                 full;                   // From etrace_fifo of etrace_fifo.v
   wire                 mem_access;             // From etrace_regs of etrace_regs.v
   wire [AW-1:0]        srcaddr;                // From etrace_fifo of etrace_fifo.v
   wire [31:0]          srcaddr_reg;            // From etrace_regs of etrace_regs.v
   wire [31:0]          stride_reg;             // From etrace_regs of etrace_regs.v
   wire                 trace_enable;           // From oh_dsync of oh_dsync.v
   wire                 update;                 // From etrace_ctrl of etrace_ctrl.v
   wire                 update2d;               // From etrace_ctrl of etrace_ctrl.v
   // End of automatics

   etrace_regs #(.ID(ID),
                 .AW(AW),
                 .PDW(PDW),
                 .VW(VW),
                 .DW(DW),
                 .PW(PW))
   etrace_regs (/*AUTOINST*/
                // Outputs
                .s_access_out           (s_access_out),
                .s_packet_out           (s_packet_out[PW-1:0]),
                .cfg_trace_enable       (cfg_trace_enable),
                .cfg_loop_enable        (cfg_loop_enable),
                .cfg_async_mode         (cfg_async_mode),
                .cfg_samplerate         (cfg_samplerate[3:0]),
                .dma_en                 (dma_en),
                .chainmode              (chainmode),
                .datamode               (datamode[1:0]),
                .ctrlmode               (ctrlmode[4:0]),
                .stride_reg             (stride_reg[31:0]),
                .dma_count_reg          (dma_count_reg[31:0]),
                .dstaddr_reg            (dstaddr_reg[31:0]),
                .srcaddr_reg            (srcaddr_reg[31:0]),
                .mem_access             (mem_access),
                // Inputs
                .clk                    (clk),
                .nreset                 (nreset),
                .s_access_in            (s_access_in),
                .s_packet_in            (s_packet_in[PW-1:0]),
                .dma_state              (dma_state[3:0]),
                .update                 (update),
                .dma_count              (dma_count[31:0]),
                .dstaddr                (dstaddr[31:0]),
                .srcaddr                (srcaddr[31:0]),
                .trace_fifo_full        (trace_fifo_full),
                .trace_fifo_empty       (trace_fifo_empty),
                .datasample_fifo        (datasample_fifo[MW-1:0]));

   etrace_ctrl #(.AW(AW),
                 .PW(PW))
   etrace_ctrl (/*AUTOINST*/
                // Outputs
                .dma_state              (dma_state[3:0]),
                .update                 (update),
                .update2d               (update2d),
                .dma_master_active      (dma_master_active),
                // Inputs
                .clk                    (clk),
                .nreset                 (nreset),
                .dma_en                 (dma_en),
                .chainmode              (chainmode),
                .dma_count              (dma_count[31:0]),
                .data_txwr_wait         (data_txwr_wait),
                .access_out             (access_out));

   // master access only when dma is active
   assign data_txwr_access = dma_master_active & access_out;

   // Ensure that all signals are synchronous with clk
   assign fifo_read_wait = ~(~dma_master_active & mem_access) & // suppress fifo read till rr ready
                           ~(cfg_loop_enable & trace_full) & // configurable: remove old and add new
                           ~(dma_master_active & ~data_txwr_wait);

   //################################
   //# SYNC CFG SIGNALS TO SAMPLE CLK
   //#################################

   oh_dsync oh_dsync(// Outputs
                     .dout    (trace_enable),
                     // Inputs
                     .clk    (trace_clk),
                     .din    (cfg_trace_enable),
                     /*AUTOINST*/
                     // Inputs
                     .nreset            (nreset));

   //###########################
   //# TIME KEEPER
   //###########################
   //count if trigger enabled and counter enabled (SW + HW)

   always @ (posedge trace_clk)
     if(~trace_enable)
       cycle_counter[DW-1:0] <= 'b0;
     else if (trace_trigger)
       cycle_counter[DW-1:0] <= cycle_counter[DW-1:0] + 1'b1;

   //###########################
   //# SAMPLING LOGIC
   //###########################

   //Change detect logic - synchronous to trace_clk
   always @ (posedge trace_clk)
     trace_vector_reg[VW-1:0] <= trace_vector[VW-1:0];

   assign change_detect = |(trace_vector_reg[VW-1:0] ^ trace_vector[VW-1:0]);

   //Sample signal - synchronous to trace_clk
   assign trace_sample = trace_enable &
                         trace_trigger &
                         change_detect &
                         ~(~cfg_loop_enable & full); // configurable: prevent sampling on full

   //Shadow Change detect logic - synchronous to clk
   always @ (posedge clk)
     begin
        trace_vector_shadow_reg[VW-1:0] <= trace_vector[VW-1:0];
        trace_fifo_full <= full;
        trace_fifo_empty <= empty;
     end

   assign change_detect_shadow = |(trace_vector_shadow_reg[VW-1:0] ^ trace_vector[VW-1:0]);

   //Shadow sample signal - synchronous to clk
   assign trace_full = trace_fifo_full & cfg_trace_enable &
             trace_trigger &
             change_detect_shadow;

   etrace_fifo #(.TARGET(TARGET),
                 .AW(AW),
                 .PDW(PDW),
                 .PW(PW),
                 .DW(DW),
                 .VW(VW),
                 .DEPTH(DEPTH))
   etrace_fifo (// Outputs
                .packet_out             (data_txwr_packet[PW-1:0]),
                /*AUTOINST*/
                // Outputs
                .dma_count              (dma_count[31:0]),
                .srcaddr                (srcaddr[AW-1:0]),
                .dstaddr                (dstaddr[AW-1:0]),
                .access_out             (access_out),
                .datasample_fifo        (datasample_fifo[MW-1:0]),
                .full                   (full),
                .empty                  (empty),
                // Inputs
                .clk                    (clk),
                .nreset                 (nreset),
                .update2d               (update2d),
                .stride_reg             (stride_reg[31:0]),
                .dma_count_reg          (dma_count_reg[31:0]),
                .srcaddr_reg            (srcaddr_reg[AW-1:0]),
                .dstaddr_reg            (dstaddr_reg[AW-1:0]),
                .fifo_read_wait         (fifo_read_wait),
                .datamode               (datamode[1:0]),
                .ctrlmode               (ctrlmode[4:0]),
                .trace_clk              (trace_clk),
                .trace_sample           (trace_sample),
                .trace_vector           (trace_vector[VW-1:0]),
                .cycle_counter          (cycle_counter[DW-1:0]));

`ifdef TARGET_SIM
   reg [31:0]         ftrace;
   reg [255:0]         tracefile;
   initial
     begin
    $sformat(tracefile,"%s%s",NAME,".trace");
        ftrace  = $fopen({tracefile}, "w");
     end

   always @ (posedge trace_clk)
     if(trace_sample)
          $fwrite(ftrace, "%h,%0d\n",trace_vector[VW-1:0],cycle_counter[DW-1:0]);

`endif //  `ifdef TARGET_SIM

endmodule // etrace

// Local Variables:
// verilog-library-directories:("." "../../emesh/hdl" "../../stdlib/rtl")
// End:
