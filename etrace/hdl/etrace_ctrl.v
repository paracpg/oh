//#############################################################################
//# Purpose: DMA etrace control                                               #
//#############################################################################
//# Author:   Peter Saunderson                                                #
//# License:  MIT (see LICENSE file in OH! repository)                        #
//#############################################################################
`include "etrace_regmap.vh"
module etrace_ctrl (/*AUTOARG*/
   // Outputs
   dma_state, update, update2d, dma_master_active,
   // Inputs
   clk, nreset, dma_en, chainmode, dma_count, data_txwr_wait,
   access_out
   );
   parameter  AW  = 32;        // address width
   parameter  PW  = 2*AW+40;   // fetch packet width
   parameter  ID  = 4'b0000;   // group id for DMA regs [10:8]

   // clk, reset, config
   input        clk;           // main clock
   input 	    nreset;        // async active low reset
   input 	    dma_en;        // dma is enabled
   input        chainmode;     // idle when done
   input [31:0] dma_count;     // current transfer count

   // master access
   input        data_txwr_wait;
   input        access_out;

   // status
   output [3:0] dma_state;         // state of dma
   output 	    update;            // update registers
   output 	    update2d;          // dma currently in outerloop (2D)
   output 	    dma_master_active; // dma_master is active

   //###########################################################################
   //# BODY
   //###########################################################################

   reg [3:0] 	 dma_state;
   wire [AW-1:0] srcaddr_out;
   wire [4:0] 	 reg_addr;
   wire 	     dma_error;
   wire 	     incount_zero;
   wire 	     outcount_zero;
   wire          manualmode;

   //###########################################
   //# STATE MACHINE
   //###########################################
   assign manualmode = 1'b1;

  `define DMA_IDLE    4'b0000 // dma idle
  `define DMA_INNER   4'b0110 // dma inner loop
  `define DMA_OUTER   4'b0111 // dma outer loop
  `define DMA_DONE    4'b1000 // dma outer loop
  `define DMA_ERROR   4'b1001 // dma error

   always @ (posedge clk or negedge nreset)
     if(!nreset)
       dma_state[3:0] <= `DMA_IDLE;
     else if(dma_error)
       dma_state[3:0] <= `DMA_ERROR;
     else
       case(dma_state[3:0])
	 `DMA_IDLE:
	   casez({dma_en,manualmode})
	     2'b0?  : dma_state[3:0]   <= `DMA_IDLE;
	     2'b11  : dma_state[3:0]   <= `DMA_INNER;
	   endcase // casez (dma_reg_write_config)
	 `DMA_INNER:
	   casez({update,incount_zero,outcount_zero})
	     3'b0?? : dma_state[3:0] <= `DMA_INNER;
	     3'b10? : dma_state[3:0] <= `DMA_INNER;
	     3'b110 : dma_state[3:0] <= `DMA_OUTER;
	     3'b111 : dma_state[3:0] <= `DMA_DONE;
	   endcase
 	 `DMA_OUTER:
	   dma_state[3:0]   <= update ? `DMA_INNER : `DMA_OUTER;
	 `DMA_DONE:
	   casez({chainmode,manualmode})
	     2'b0? : dma_state[3:0]  <= `DMA_DONE;
	     2'b11 : dma_state[3:0]  <= `DMA_IDLE;
	   endcase
	 `DMA_ERROR:
	   dma_state[3:0] <= dma_en ? `DMA_ERROR: `DMA_IDLE;
       endcase

   //###########################################
   //# ACTIVE SIGNALS
   //###########################################

   assign dma_error     = 1'b0;  //TODO: define error conditions

   assign update        = ~data_txwr_wait & dma_master_active & access_out;

   assign update2d      = update & (dma_state[3:0]==`DMA_OUTER);

   assign dma_master_active = dma_en &
			  ((dma_state[3:0]==`DMA_INNER) |
			   (dma_state[3:0]==`DMA_OUTER));

   assign incount_zero  = ~(|dma_count[15:0]);

   assign outcount_zero = ~(|dma_count[31:16]);

endmodule // etrace_ctrl
// Local Variables:
// verilog-library-directories:("." "../hdl" "../../stdlib/rtl" "../../emesh/hdl")
// End:
