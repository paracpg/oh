module axi_etrace (/*AUTOARG*/
   // Outputs
   m_axi_awid, m_axi_awaddr, m_axi_awlen, m_axi_awsize, m_axi_awburst,
   m_axi_awlock, m_axi_awcache, m_axi_awprot, m_axi_awqos,
   m_axi_awregion, m_axi_awvalid, m_axi_wid, m_axi_wdata, m_axi_wstrb,
   m_axi_wlast, m_axi_wvalid, m_axi_bready, m_axi_arid, m_axi_araddr,
   m_axi_arlen, m_axi_arsize, m_axi_arburst, m_axi_arlock,
   m_axi_arcache, m_axi_arprot, m_axi_arqos, m_axi_arvalid,
   m_axi_rready, s_axi_arregion, s_axi_arready, s_axi_awready,
   s_axi_bid, s_axi_bresp, s_axi_bvalid, s_axi_rid, s_axi_rdata,
   s_axi_rlast, s_axi_rresp, s_axi_rvalid, s_axi_wready,
   constant_zero, constant_one,
   // Inputs
   trace_clk, trace_trigger, trace_vector, m_axi_aclk, m_axi_aresetn,
   m_axi_awready, m_axi_wready, m_axi_bid, m_axi_bresp, m_axi_bvalid,
   m_axi_arregion, m_axi_arready, m_axi_rid, m_axi_rdata, m_axi_rresp,
   m_axi_rlast, m_axi_rvalid, clk, nreset, s_axi_arid, s_axi_araddr,
   s_axi_arburst, s_axi_arcache, s_axi_arlock, s_axi_arlen,
   s_axi_arprot, s_axi_arqos, s_axi_arsize, s_axi_arvalid, s_axi_awid,
   s_axi_awaddr, s_axi_awburst, s_axi_awcache, s_axi_awlock,
   s_axi_awlen, s_axi_awprot, s_axi_awqos, s_axi_awregion,
   s_axi_awsize, s_axi_awvalid, s_axi_bready, s_axi_rready, s_axi_wid,
   s_axi_wdata, s_axi_wlast, s_axi_wstrb, s_axi_wvalid
   );

   parameter VW          = 16;            // 32 width of vector to sample
   parameter CW          = 16;            // 32 width of sample counter
   parameter AW          = 32;
   parameter DW          = 32;            // 32 saxi data width
   parameter MDW         = 32;            // 32 maxi data width
   parameter PW          = 104;           // packet width
   parameter ID          = 12'h810;
   parameter S_IDW       = 12;            // ID width for S_AXI
   parameter M_IDW       = 6;             // ID width for M_AXI

   //Logic analyzer interface
   input            trace_clk;
   input            trace_trigger;
   input [VW-1:0]   trace_vector;

   //########################
   //AXI MASTER INTERFACE
   //########################

   //clk+reset
   input            m_axi_aclk;
   input            m_axi_aresetn; // global reset singal.

   //Write address channel
   output [M_IDW-1:0] m_axi_awid;    // write address ID
   output [31 : 0]    m_axi_awaddr;  // master interface write address
   output [7 : 0]     m_axi_awlen;   // burst length.
   output [2 : 0]     m_axi_awsize;  // burst size.
   output [1 : 0]     m_axi_awburst; // burst type.
   output             m_axi_awlock;  // lock type
   output [3 : 0]     m_axi_awcache; // memory type.
   output [2 : 0]     m_axi_awprot;  // protection type.
   output [3 : 0]     m_axi_awqos;   // AXI4 quality of service
   output [3:0]       m_axi_awregion; // AXI4
   output           m_axi_awvalid; // write address valid

   input           m_axi_awready; // write address ready

   //Write data channel
   output [M_IDW-1:0] m_axi_wid;
   output [63 : 0]    m_axi_wdata;   // master interface write data.
   output [MDW/8-1 : 0]     m_axi_wstrb;   // byte write strobes
   output           m_axi_wlast;   // last transfer in a write burst.
   output           m_axi_wvalid;  // indicates data is ready to go
   input           m_axi_wready;  // slave is ready for data

   //Write response channel
   input [M_IDW-1:0]  m_axi_bid;
   input [1 : 0]      m_axi_bresp;   // status of the write transaction.
   input           m_axi_bvalid;  // valid write response
   output           m_axi_bready;  // master can accept write response.

   //Read address channel
   output [M_IDW-1:0] m_axi_arid;    // read address ID
   output [AW-1 : 0]  m_axi_araddr;  // initial address of a read burst
   output [7 : 0]     m_axi_arlen;   // burst length
   output [2 : 0]     m_axi_arsize;  // burst size
   output [1 : 0]     m_axi_arburst; // burst type
   output             m_axi_arlock;  // lock type
   output [3 : 0]     m_axi_arcache; // memory type
   output [2 : 0]     m_axi_arprot;  // protection type
   output [3 : 0]     m_axi_arqos;   // AXI4
   output             m_axi_arvalid; // read address and control is valid
   input [3:0]        m_axi_arregion;// AXI4
   input              m_axi_arready; // slave is ready to accept an address

   //Read data channel
   input [M_IDW-1:0]  m_axi_rid;
   input [MDW-1 : 0]  m_axi_rdata;   // master read data
   input [1 : 0]      m_axi_rresp;   // status of the read transfer
   input              m_axi_rlast;   // signals last transfer in a read burst
   input              m_axi_rvalid;  // signaling the required read data
   output             m_axi_rready;  // master can accept the readback data

   //########################
   //AXI SLAVE INTERFACE
   //########################

   //clk+reset
   input     clk;
   input     nreset;

   //Read address channel
   input [S_IDW-1:0]  s_axi_arid;    //write address ID
   input [31:0]       s_axi_araddr;
   input [1:0]           s_axi_arburst;
   input [3:0]           s_axi_arcache;
   input            s_axi_arlock;
   input [7:0]           s_axi_arlen;
   input [2:0]           s_axi_arprot;
   input [3:0]           s_axi_arqos; // AXI4
   output [3:0]        s_axi_arregion; // AXI4
   output           s_axi_arready;
   input [2:0]           s_axi_arsize;
   input           s_axi_arvalid;

   //Write address channel
   input [S_IDW-1:0]  s_axi_awid;    //write address ID
   input [31:0]       s_axi_awaddr;
   input [1:0]           s_axi_awburst;
   input [3:0]           s_axi_awcache;
   input      s_axi_awlock;
   input [7:0]           s_axi_awlen;
   input [2:0]           s_axi_awprot;
   input [3:0]           s_axi_awqos; // AXI4
   input [3:0]        s_axi_awregion; // AXI4
   input [2:0]           s_axi_awsize;
   input           s_axi_awvalid;
   output           s_axi_awready;

   //Buffered write response channel
   output [S_IDW-1:0] s_axi_bid;    //write address ID
   output [1:0]       s_axi_bresp;
   output           s_axi_bvalid;
   input           s_axi_bready;

   //Read channel
   output [S_IDW-1:0] s_axi_rid;    //write address ID
   output [DW-1:0]      s_axi_rdata;
   output           s_axi_rlast;
   output [1:0]       s_axi_rresp;
   output           s_axi_rvalid;
   input           s_axi_rready;

   //Write channel
   input [S_IDW-1:0]  s_axi_wid;    //write address ID
   input [DW-1:0]       s_axi_wdata;
   input           s_axi_wlast;
   input [3:0]           s_axi_wstrb;
   input           s_axi_wvalid;
   output           s_axi_wready;

   output		constant_zero;
   output		constant_one;

   // Master write packet
   wire          data_txwr_access;
   wire          data_txwr_wait;
   wire [PW-1:0] data_txwr_packet;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 s_access_in;            // From emesh_mux of emesh_mux.v
   wire                 s_access_out;           // From etrace of etrace.v
   wire [PW-1:0]        s_packet_in;            // From emesh_mux of emesh_mux.v
   wire [PW-1:0]        s_packet_out;           // From etrace of etrace.v
   wire                 s_rd_access;            // From esaxi of esaxi.v
   wire [PW-1:0]        s_rd_packet;            // From esaxi of esaxi.v
   wire                 s_rd_wait;              // From emesh_mux of emesh_mux.v
   wire                 s_rr_wait;              // From esaxi of esaxi.v
   wire                 s_wr_access;            // From esaxi of esaxi.v
   wire [PW-1:0]        s_wr_packet;            // From esaxi of esaxi.v
   wire                 s_wr_wait;              // From emesh_mux of emesh_mux.v
   // End of automatics

   assign constant_zero = 1'b0;
   assign constant_one = 1'b1;

   emaxi #(.AW(AW),
           .DW(MDW),
           .PW(PW))
   emaxi ( // Inputs
           .rd_access                   (1'b0),//no reads generated, just push
           .rd_packet                   ({(PW){1'b0}}),
           .rd_wait                     (),
           .rr_access                   (),//no reads, so no responses
           .rr_packet                   (),
           .rr_wait                     (1'b0),
           .wr_access                   (data_txwr_access),
           .wr_packet                   (data_txwr_packet[PW-1:0]),

           .m_axi_rdata                 (m_axi_rdata[MDW-1:0]),
           // Outputs
           .wr_wait                     (data_txwr_wait),
           .m_axi_wstrb                 (m_axi_wstrb[MDW/8-1:0]),
           .m_axi_wdata                 (m_axi_wdata[MDW-1:0]),
           /*AUTOINST*/
          // Outputs
          .m_axi_awid                   (m_axi_awid[M_IDW-1:0]),
          .m_axi_awaddr                 (m_axi_awaddr[AW-1:0]),
          .m_axi_awlen                  (m_axi_awlen[7:0]),
          .m_axi_awsize                 (m_axi_awsize[2:0]),
          .m_axi_awburst                (m_axi_awburst[1:0]),
          .m_axi_awlock                 (m_axi_awlock),
          .m_axi_awcache                (m_axi_awcache[3:0]),
          .m_axi_awprot                 (m_axi_awprot[2:0]),
          .m_axi_awqos                  (m_axi_awqos[3:0]),
          .m_axi_awregion               (m_axi_awregion[3:0]),
          .m_axi_awvalid                (m_axi_awvalid),
          .m_axi_wid                    (m_axi_wid[M_IDW-1:0]),
          .m_axi_wlast                  (m_axi_wlast),
          .m_axi_wvalid                 (m_axi_wvalid),
          .m_axi_bready                 (m_axi_bready),
          .m_axi_arid                   (m_axi_arid[M_IDW-1:0]),
          .m_axi_araddr                 (m_axi_araddr[AW-1:0]),
          .m_axi_arlen                  (m_axi_arlen[7:0]),
          .m_axi_arsize                 (m_axi_arsize[2:0]),
          .m_axi_arburst                (m_axi_arburst[1:0]),
          .m_axi_arlock                 (m_axi_arlock),
          .m_axi_arcache                (m_axi_arcache[3:0]),
          .m_axi_arprot                 (m_axi_arprot[2:0]),
          .m_axi_arqos                  (m_axi_arqos[3:0]),
          .m_axi_arvalid                (m_axi_arvalid),
          .m_axi_rready                 (m_axi_rready),
          // Inputs
          .m_axi_aclk                   (m_axi_aclk),
          .m_axi_aresetn                (m_axi_aresetn),
          .m_axi_awready                (m_axi_awready),
          .m_axi_wready                 (m_axi_wready),
          .m_axi_bid                    (m_axi_bid[M_IDW-1:0]),
          .m_axi_bresp                  (m_axi_bresp[1:0]),
          .m_axi_bvalid                 (m_axi_bvalid),
          .m_axi_arready                (m_axi_arready),
          .m_axi_arregion               (m_axi_arregion[3:0]),
          .m_axi_rid                    (m_axi_rid[M_IDW-1:0]),
          .m_axi_rresp                  (m_axi_rresp[1:0]),
          .m_axi_rlast                  (m_axi_rlast),
          .m_axi_rvalid                 (m_axi_rvalid));

   esaxi esaxi (// Outputs
                .wr_access              (s_wr_access),
                .wr_packet              (s_wr_packet[PW-1:0]),
                .rd_wait                (s_rd_wait),
                .rr_access              (s_access_out),
                .rr_packet              (s_packet_out[PW-1:0]),
                // Inputs
                .s_axi_aclk             (clk),
                .s_axi_aresetn          (nreset),
                .wr_wait                (s_wr_wait),
                .rd_access              (s_rd_access),
                .rd_packet              (s_rd_packet[PW-1:0]),
                .rr_wait                (s_rr_wait),
                /*AUTOINST*/
                // Outputs
                .s_axi_arregion         (s_axi_arregion[3:0]),
                .s_axi_arready          (s_axi_arready),
                .s_axi_awready          (s_axi_awready),
                .s_axi_bid              (s_axi_bid[S_IDW-1:0]),
                .s_axi_bresp            (s_axi_bresp[1:0]),
                .s_axi_bvalid           (s_axi_bvalid),
                .s_axi_rid              (s_axi_rid[S_IDW-1:0]),
                .s_axi_rdata            (s_axi_rdata[DW-1:0]),
                .s_axi_rlast            (s_axi_rlast),
                .s_axi_rresp            (s_axi_rresp[1:0]),
                .s_axi_rvalid           (s_axi_rvalid),
                .s_axi_wready           (s_axi_wready),
                // Inputs
                .s_axi_arid             (s_axi_arid[S_IDW-1:0]),
                .s_axi_araddr           (s_axi_araddr[AW-1:0]),
                .s_axi_arburst          (s_axi_arburst[1:0]),
                .s_axi_arcache          (s_axi_arcache[3:0]),
                .s_axi_arlock           (s_axi_arlock),
                .s_axi_arlen            (s_axi_arlen[7:0]),
                .s_axi_arprot           (s_axi_arprot[2:0]),
                .s_axi_arqos            (s_axi_arqos[3:0]),
                .s_axi_arsize           (s_axi_arsize[2:0]),
                .s_axi_arvalid          (s_axi_arvalid),
                .s_axi_awid             (s_axi_awid[S_IDW-1:0]),
                .s_axi_awaddr           (s_axi_awaddr[AW-1:0]),
                .s_axi_awburst          (s_axi_awburst[1:0]),
                .s_axi_awcache          (s_axi_awcache[3:0]),
                .s_axi_awlock           (s_axi_awlock),
                .s_axi_awlen            (s_axi_awlen[7:0]),
                .s_axi_awprot           (s_axi_awprot[2:0]),
                .s_axi_awqos            (s_axi_awqos[3:0]),
                .s_axi_awregion         (s_axi_awregion[3:0]),
                .s_axi_awsize           (s_axi_awsize[2:0]),
                .s_axi_awvalid          (s_axi_awvalid),
                .s_axi_bready           (s_axi_bready),
                .s_axi_rready           (s_axi_rready),
                .s_axi_wid              (s_axi_wid[S_IDW-1:0]),
                .s_axi_wdata            (s_axi_wdata[DW-1:0]),
                .s_axi_wlast            (s_axi_wlast),
                .s_axi_wstrb            (s_axi_wstrb[DW/8-1:0]),
                .s_axi_wvalid           (s_axi_wvalid));


   //mux the read/write together
   emesh_mux #(.N(2),.AW(AW))
   emesh_mux (// Outputs
              .wait_out                 ({s_rd_wait, s_wr_wait}),
              .access_out               (s_access_in),
              .packet_out               (s_packet_in[PW-1:0]),
              // Inputs
              .access_in                ({s_rd_access, s_wr_access}),
              .packet_in                ({s_rd_packet[PW-1:0], s_wr_packet[PW-1:0]}),
              .wait_in                  (s_rr_wait)
              /*AUTOINST*/);

   //tracing unit
   etrace #(.VW(VW),.DW(CW),.AW(AW),.PDW(DW),.ID(ID))
   etrace (// Outputs
           .data_txwr_access            (data_txwr_access),
           .data_txwr_packet            (data_txwr_packet[PW-1:0]),
           // Inputs
           .clk                         (clk),
           .nreset                      (nreset),
           .data_txwr_wait              (data_txwr_wait),
           /*AUTOINST*/
           // Outputs
           .s_access_out                (s_access_out),
           .s_packet_out                (s_packet_out[PW-1:0]),
           // Inputs
           .trace_clk                   (trace_clk),
           .trace_trigger               (trace_trigger),
           .trace_vector                (trace_vector[VW-1:0]),
           .s_access_in                 (s_access_in),
           .s_packet_in                 (s_packet_in[PW-1:0]));

endmodule // axi_etrace
// Local Variables:
// verilog-library-directories:("." "../../axi/hdl" "../../emesh/hdl")
// End:
