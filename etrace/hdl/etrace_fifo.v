//#############################################################################
//# Purpose: DMA etrace data path (based on edma_dp)                          #
//#############################################################################
//# Author:   Peter Saunderson                                                #
//# License:  MIT (see LICENSE file in OH! repository)                        #
//#############################################################################
module etrace_fifo (/*AUTOARG*/
   // Outputs
   dma_count, srcaddr, dstaddr, access_out, packet_out,
   datasample_fifo, full, empty,
   // Inputs
   clk, nreset, update2d, stride_reg, dma_count_reg, srcaddr_reg,
   dstaddr_reg, fifo_read_wait, datamode, ctrlmode, trace_clk,
   trace_sample, trace_vector, cycle_counter
   );
   parameter TARGET = "GENERIC";  // XILINX,ALTERA,GENERIC
   parameter VW     = 32;         // width of vector to sample
   parameter DW     = 32;         // trace sample (data and time) width
   parameter AW     = 32;         // dma_counter width
   parameter PDW    = 32;         // packet data width
   parameter DEPTH  = 1024;       // memory depth
   parameter MW     = VW+DW;      // memory width should be 64,128,256
   parameter PW     = 2*AW+40;    // emesh packet width

   // clk, reset, config
   input           clk;           // main clock
   input 	       nreset;        // async active low reset
   input 	       update2d;      // outer loop transfer

   // data registers
   input [31:0]    stride_reg;    // transfer stride
   input [31:0]    dma_count_reg; // starting dma_count
   input [AW-1:0]  srcaddr_reg;   // starting source address
   input [AW-1:0]  dstaddr_reg;   // starting destination address

   // output to register file
   output [31:0]   dma_count;     // current dma_count
   output [AW-1:0] srcaddr;       // current source address
   output [AW-1:0] dstaddr;       // current source address

   // output packet
   output 	       access_out;
   output [PW-1:0] packet_out;     // output packet (with address)
   input 	       fifo_read_wait; // pushback

   input [1:0]     datamode;
   input [4:0]     ctrlmode;

   // output sample
   output [MW-1:0] datasample_fifo;

   // datapath interface data
   input            trace_clk;
   input            trace_sample;
   input [VW-1:0]   trace_vector;
   input [DW-1:0]   cycle_counter;

   // trace fifo state
   output           full;
   output           empty;

   //######################################################################
   //# BODY
   //######################################################################

   // wires
   wire [MW-1:0]   datasample_fifo;
   wire [AW-1:0]   data_fifo;
   wire [1:0] 	   datamode_fifo;
   wire [4:0] 	   ctrlmode_fifo;
   wire [AW-1:0]   dstaddr_fifo;
   wire [AW-1:0]   srcaddr_fifo;
   wire 	       write_fifo;
   wire [PW-1:0]   packet;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 prog_full;              // From fifo of oh_fifo_cdc.v
   wire                 wait_out;               // From fifo of oh_fifo_cdc.v
   // End of automatics
   /*AUTOINPUT*/

   //################################
   //# DMA_COUNT
   //################################
   // Note: outer loop (update2d) not used
   assign dma_count[31:0] = update2d ? {(dma_count_reg[31:16] - 1'b1), dma_count_reg[15:0]} :
		                     dma_count_reg[31:0] - 1'b1;

   //################################
   //# SRCADDR
   //################################
   // Note: outer loop not used
   // TODO use srcaddr for cache access
   assign srcaddr[AW-1:0] = srcaddr_reg[AW-1:0] +
			    {{(AW-16){stride_reg[15]}}, stride_reg[15:0]};  // update2d ?

   //################################
   //# DSTADDR
   //################################
   // Note: outer loop not used
   assign dstaddr[AW-1:0] = dstaddr_reg[AW-1:0] +
			    {{(AW-16){stride_reg[31]}}, stride_reg[31:16]};  // update2d ?

   //################################
   //# pipeline the data with fifo
   //################################
   oh_fifo_cdc #(.TARGET(TARGET),
                 .DW(MW),
                 .DEPTH(DEPTH))
   fifo (// Outputs
         .packet_out                (datasample_fifo[MW-1:0]),
         //Inputs
         .wait_in                   (fifo_read_wait),
         .packet_in                 ({trace_vector[VW-1:0], cycle_counter[DW-1:0]}),
         .access_in                 (trace_sample),
         .clk_in                    (trace_clk),
         .clk_out                   (clk),
         /*AUTOINST*/
         // Outputs
         .wait_out                      (wait_out),
         .access_out                    (access_out),
         .prog_full                     (prog_full),
         .full                          (full),
         .empty                         (empty),
         // Inputs
         .nreset                        (nreset));

   assign write_fifo = 1'b1;

   // 64 bit data is split between data and srcaddr


   //
   generate
      if (MW == 32)
        begin
        assign data_fifo[AW-1:0] = {{(AW-MW){1'b0}}, datasample_fifo[MW-1:0]};
        assign srcaddr_fifo[AW-1:0] = {(AW){1'b0}};
        end
      else // MW == 64
        begin
        assign data_fifo[31:0] = datasample_fifo[DW-1:0];
        assign srcaddr_fifo[AW-1:0] = {{(AW-DW){1'b0}}, datasample_fifo[MW-1:DW]};
        end
   endgenerate

   assign dstaddr_fifo[AW-1:0] = dstaddr[AW-1:0];
   assign datamode_fifo[1:0] = datamode[1:0];
   assign ctrlmode_fifo[4:0] = ctrlmode[4:0];

   // constructing output packet
   /*emesh2packet AUTO_TEMPLATE (.\(.*\)_out (\1_fifo[]),
        );
     */
   emesh2packet #(.AW(AW),
		  .PW(PW))
   e2p (.packet_out			            (packet_out[PW-1:0]),
     /*AUTOINST*/
        // Inputs
        .write_out                      (write_fifo),            // Templated
        .datamode_out                   (datamode_fifo[1:0]),    // Templated
        .ctrlmode_out                   (ctrlmode_fifo[4:0]),    // Templated
        .dstaddr_out                    (dstaddr_fifo[AW-1:0]),  // Templated
        .data_out                       (data_fifo[AW-1:0]),     // Templated
        .srcaddr_out                    (srcaddr_fifo[AW-1:0])); // Templated

endmodule // edma_dp
// Local Variables:
// verilog-library-directories:("." "../../emesh/hdl" "../../stdlib/rtl")
// End:
