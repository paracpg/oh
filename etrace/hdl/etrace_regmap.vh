`define  ETRACE_CFG       6'd0  // configuration
`define  ETRACE_STRIDE    6'd1  // stride
`define  ETRACE_COUNT     6'd2  // count
`define  ETRACE_SRCADDR   6'd4  // source address
`define  ETRACE_DSTADDR   6'd5  // dma destination address
`define  ETRACE_SRCADDR64 6'd6  // extended source address (64b)
`define  ETRACE_DSTADDR64 6'd7  // extended destinationa ddress (64b)
`define  ETRACE_STATUS    6'd8  // status register
`define  ETRACE_REGS      4'hF  // group decode
`define  ETRACE_MEM       4'hA  // group decode
