`include "elink_regmap.vh"
module dut(/*AUTOARG*/
   // Outputs
   dut_active, clkout, wait_out, access_out, packet_out,
   // Inputs
   clk1, clk2, nreset, vdd, vss, access_in, packet_in, wait_in
   );

   //##########################################################################
   //# INTERFACE
   //##########################################################################

   parameter VW     = 32;
   parameter AW     = 32;
   parameter DW     = 32;
   parameter MDW    = VW+VW;
   parameter LW     = 64; // size of packet data
   parameter STRBW  = DW/8;  // bytes in DW
   parameter MSTRBW = MDW/8; // bytes in DW
   parameter ID     = 12'h810;
   parameter S_IDW  = 12;
   parameter M_IDW  = 6;
   parameter PW     = 2*AW + 40;
   parameter N      = 1;
   parameter RETURN_ADDR = {ID,
                            `EGROUP_RR,
                            16'b0};      // axi return addr

   //clock,reset
   input             clk1;
   input             clk2;
   input             nreset;
   input [N*N-1:0]   vdd;
   input             vss;
   output            dut_active;
   output            clkout;

   //Stimulus Driven Transaction
   input [N-1:0]     access_in;
   input [N*PW-1:0]  packet_in;
   output [N-1:0]    wait_out;

   //DUT driven transaction
   output [N-1:0]    access_out;
   output [N*PW-1:0] packet_out;
   input [N-1:0]     wait_in;

   //##########################################################################
   //#BODY
   //##########################################################################

   reg               dut_active;

   wire              read_in;
   wire              write_in;

   wire [M_IDW-1:0]  m_axi_wid;
   wire [M_IDW-1:0]  m_axi_awid;

   wire              wr_wait;
   wire              rd_wait;

   wire [MDW-1:0]        dma_axi_rdata;

   /*AUTOINPUT*/
   /*AUTOINOUT*/
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 constant_one;           // From axi_etrace of axi_etrace.v
   wire                 constant_zero;          // From axi_etrace of axi_etrace.v
   wire [AW-1:0]        dma_axi_araddr;         // From axi_etrace of axi_etrace.v
   wire [1:0]           dma_axi_arburst;        // From axi_etrace of axi_etrace.v
   wire [3:0]           dma_axi_arcache;        // From axi_etrace of axi_etrace.v
   wire [M_IDW-1:0]     dma_axi_arid;           // From axi_etrace of axi_etrace.v
   wire [7:0]           dma_axi_arlen;          // From axi_etrace of axi_etrace.v
   wire                 dma_axi_arlock;         // From axi_etrace of axi_etrace.v
   wire [2:0]           dma_axi_arprot;         // From axi_etrace of axi_etrace.v
   wire [3:0]           dma_axi_arqos;          // From axi_etrace of axi_etrace.v
   wire                 dma_axi_arready;        // From esaxi of esaxi.v
   wire [3:0]           dma_axi_arregion;       // From esaxi of esaxi.v
   wire [2:0]           dma_axi_arsize;         // From axi_etrace of axi_etrace.v
   wire                 dma_axi_arvalid;        // From axi_etrace of axi_etrace.v
   wire [31:0]          dma_axi_awaddr;         // From axi_etrace of axi_etrace.v
   wire [1:0]           dma_axi_awburst;        // From axi_etrace of axi_etrace.v
   wire [3:0]           dma_axi_awcache;        // From axi_etrace of axi_etrace.v
   wire [M_IDW-1:0]     dma_axi_awid;           // From axi_etrace of axi_etrace.v
   wire [7:0]           dma_axi_awlen;          // From axi_etrace of axi_etrace.v
   wire                 dma_axi_awlock;         // From axi_etrace of axi_etrace.v
   wire [2:0]           dma_axi_awprot;         // From axi_etrace of axi_etrace.v
   wire [3:0]           dma_axi_awqos;          // From axi_etrace of axi_etrace.v
   wire                 dma_axi_awready;        // From esaxi of esaxi.v
   wire [3:0]           dma_axi_awregion;       // From axi_etrace of axi_etrace.v
   wire [2:0]           dma_axi_awsize;         // From axi_etrace of axi_etrace.v
   wire                 dma_axi_awvalid;        // From axi_etrace of axi_etrace.v
   wire [S_IDW-1:0]     dma_axi_bid;            // From esaxi of esaxi.v
   wire                 dma_axi_bready;         // From axi_etrace of axi_etrace.v
   wire [1:0]           dma_axi_bresp;          // From esaxi of esaxi.v
   wire                 dma_axi_bvalid;         // From esaxi of esaxi.v
   wire [S_IDW-1:0]     dma_axi_rid;            // From esaxi of esaxi.v
   wire                 dma_axi_rlast;          // From esaxi of esaxi.v
   wire                 dma_axi_rready;         // From axi_etrace of axi_etrace.v
   wire [1:0]           dma_axi_rresp;          // From esaxi of esaxi.v
   wire                 dma_axi_rvalid;         // From esaxi of esaxi.v
   wire [63:0]          dma_axi_wdata;          // From axi_etrace of axi_etrace.v
   wire [M_IDW-1:0]     dma_axi_wid;            // From axi_etrace of axi_etrace.v
   wire                 dma_axi_wlast;          // From axi_etrace of axi_etrace.v
   wire                 dma_axi_wready;         // From esaxi of esaxi.v
   wire [MSTRBW-1:0]    dma_axi_wstrb;          // From axi_etrace of axi_etrace.v
   wire                 dma_axi_wvalid;         // From axi_etrace of axi_etrace.v
   wire [AW-1:0]        m_axi_araddr;           // From emaxi of emaxi.v
   wire [1:0]           m_axi_arburst;          // From emaxi of emaxi.v
   wire [3:0]           m_axi_arcache;          // From emaxi of emaxi.v
   wire [M_IDW-1:0]     m_axi_arid;             // From emaxi of emaxi.v
   wire [7:0]           m_axi_arlen;            // From emaxi of emaxi.v
   wire                 m_axi_arlock;           // From emaxi of emaxi.v
   wire [2:0]           m_axi_arprot;           // From emaxi of emaxi.v
   wire [3:0]           m_axi_arqos;            // From emaxi of emaxi.v
   wire                 m_axi_arready;          // From axi_etrace of axi_etrace.v
   wire [3:0]           m_axi_arregion;         // From axi_etrace of axi_etrace.v
   wire [2:0]           m_axi_arsize;           // From emaxi of emaxi.v
   wire                 m_axi_arvalid;          // From emaxi of emaxi.v
   wire [AW-1:0]        m_axi_awaddr;           // From emaxi of emaxi.v
   wire [1:0]           m_axi_awburst;          // From emaxi of emaxi.v
   wire [3:0]           m_axi_awcache;          // From emaxi of emaxi.v
   wire [7:0]           m_axi_awlen;            // From emaxi of emaxi.v
   wire                 m_axi_awlock;           // From emaxi of emaxi.v
   wire [2:0]           m_axi_awprot;           // From emaxi of emaxi.v
   wire [3:0]           m_axi_awqos;            // From emaxi of emaxi.v
   wire                 m_axi_awready;          // From axi_etrace of axi_etrace.v
   wire [3:0]           m_axi_awregion;         // From emaxi of emaxi.v
   wire [2:0]           m_axi_awsize;           // From emaxi of emaxi.v
   wire                 m_axi_awvalid;          // From emaxi of emaxi.v
   wire [S_IDW-1:0]     m_axi_bid;              // From axi_etrace of axi_etrace.v
   wire                 m_axi_bready;           // From emaxi of emaxi.v
   wire [1:0]           m_axi_bresp;            // From axi_etrace of axi_etrace.v
   wire                 m_axi_bvalid;           // From axi_etrace of axi_etrace.v
   wire [DW-1:0]        m_axi_rdata;            // From axi_etrace of axi_etrace.v
   wire [S_IDW-1:0]     m_axi_rid;              // From axi_etrace of axi_etrace.v
   wire                 m_axi_rlast;            // From axi_etrace of axi_etrace.v
   wire                 m_axi_rready;           // From emaxi of emaxi.v
   wire [1:0]           m_axi_rresp;            // From axi_etrace of axi_etrace.v
   wire                 m_axi_rvalid;           // From axi_etrace of axi_etrace.v
   wire [DW-1:0]        m_axi_wdata;            // From emaxi of emaxi.v
   wire                 m_axi_wlast;            // From emaxi of emaxi.v
   wire                 m_axi_wready;           // From axi_etrace of axi_etrace.v
   wire [STRBW-1:0]     m_axi_wstrb;            // From emaxi of emaxi.v
   wire                 m_axi_wvalid;           // From emaxi of emaxi.v
   // End of automatics

   assign clkout = clk1;

   always @ (posedge clk1 or negedge nreset)
     if(!nreset)
       dut_active <= 1'b0;
     else if(dut_active | m_axi_wstrb[0])
       dut_active <= 1'b1;


   //######################################################################
   //AXI MASTER
   //######################################################################

   //Split stimulus to read/write
   assign wait_out = wr_wait | rd_wait;
   assign write_in = access_in & packet_in[0];
   assign read_in  = access_in & ~packet_in[0];

   emaxi #(.M_IDW(M_IDW))
   emaxi (// Inputs
          .m_axi_aclk                   (clk1),
          .m_axi_aresetn                (nreset),
          .wr_access                    (write_in),
          .wr_packet                    (packet_in[PW-1:0]),
          .rd_access                    (read_in),
          .rd_packet                    (packet_in[PW-1:0]),
          .rr_wait                      (wait_in),
          // Outputs
          .wr_wait                      (wr_wait),
          .rd_wait                      (rd_wait),
          .rr_access                    (access_out),
          .rr_packet                    (packet_out[PW-1:0]),
          /*AUTOINST*/
          // Outputs
          .m_axi_awid                   (m_axi_awid[M_IDW-1:0]),
          .m_axi_awaddr                 (m_axi_awaddr[AW-1:0]),
          .m_axi_awlen                  (m_axi_awlen[7:0]),
          .m_axi_awsize                 (m_axi_awsize[2:0]),
          .m_axi_awburst                (m_axi_awburst[1:0]),
          .m_axi_awlock                 (m_axi_awlock),
          .m_axi_awcache                (m_axi_awcache[3:0]),
          .m_axi_awprot                 (m_axi_awprot[2:0]),
          .m_axi_awqos                  (m_axi_awqos[3:0]),
          .m_axi_awregion               (m_axi_awregion[3:0]),
          .m_axi_awvalid                (m_axi_awvalid),
          .m_axi_wid                    (m_axi_wid[M_IDW-1:0]),
          .m_axi_wdata                  (m_axi_wdata[DW-1:0]),
          .m_axi_wstrb                  (m_axi_wstrb[STRBW-1:0]),
          .m_axi_wlast                  (m_axi_wlast),
          .m_axi_wvalid                 (m_axi_wvalid),
          .m_axi_bready                 (m_axi_bready),
          .m_axi_arid                   (m_axi_arid[M_IDW-1:0]),
          .m_axi_araddr                 (m_axi_araddr[AW-1:0]),
          .m_axi_arlen                  (m_axi_arlen[7:0]),
          .m_axi_arsize                 (m_axi_arsize[2:0]),
          .m_axi_arburst                (m_axi_arburst[1:0]),
          .m_axi_arlock                 (m_axi_arlock),
          .m_axi_arcache                (m_axi_arcache[3:0]),
          .m_axi_arprot                 (m_axi_arprot[2:0]),
          .m_axi_arqos                  (m_axi_arqos[3:0]),
          .m_axi_arvalid                (m_axi_arvalid),
          .m_axi_rready                 (m_axi_rready),
          // Inputs
          .m_axi_awready                (m_axi_awready),
          .m_axi_wready                 (m_axi_wready),
          .m_axi_bid                    (m_axi_bid[M_IDW-1:0]),
          .m_axi_bresp                  (m_axi_bresp[1:0]),
          .m_axi_bvalid                 (m_axi_bvalid),
          .m_axi_arready                (m_axi_arready),
          .m_axi_arregion               (m_axi_arregion[3:0]),
          .m_axi_rid                    (m_axi_rid[M_IDW-1:0]),
          .m_axi_rdata                  (m_axi_rdata[DW-1:0]),
          .m_axi_rresp                  (m_axi_rresp[1:0]),
          .m_axi_rlast                  (m_axi_rlast),
          .m_axi_rvalid                 (m_axi_rvalid));

   //TRACE
   /*axi_etrace AUTO_TEMPLATE (//Stimulus
    .s_\(.*\)     (m_\1[]),
    .m_\(.*\)     (dma_\1[]),
    .wr_\(.*\)     (dma_wr_\1[]),
    .rd_\(.*\)     (dma_rd_\1[]),
    .rr_\(.*\)     (dma_rr_\1[]),
    );
    */
   axi_etrace #(.ID(ID),
                .VW(VW),
                .CW(VW),
                .MDW(MDW))
   axi_etrace (// Inputs
               .trace_vector            (packet_in[71+VW:72]),
               .trace_trigger           (1'b1),
               .trace_clk               (clk1),
               .clk                     (clk1),
               .m_axi_aclk              (clk1),
               .m_axi_aresetn           (nreset),
               /*AUTOINST*/
               // Outputs
               .m_axi_awid              (dma_axi_awid[M_IDW-1:0]), // Templated
               .m_axi_awaddr            (dma_axi_awaddr[31:0]),  // Templated
               .m_axi_awlen             (dma_axi_awlen[7:0]),    // Templated
               .m_axi_awsize            (dma_axi_awsize[2:0]),   // Templated
               .m_axi_awburst           (dma_axi_awburst[1:0]),  // Templated
               .m_axi_awlock            (dma_axi_awlock),        // Templated
               .m_axi_awcache           (dma_axi_awcache[3:0]),  // Templated
               .m_axi_awprot            (dma_axi_awprot[2:0]),   // Templated
               .m_axi_awqos             (dma_axi_awqos[3:0]),    // Templated
               .m_axi_awregion          (dma_axi_awregion[3:0]), // Templated
               .m_axi_awvalid           (dma_axi_awvalid),       // Templated
               .m_axi_wid               (dma_axi_wid[M_IDW-1:0]), // Templated
               .m_axi_wdata             (dma_axi_wdata[63:0]),   // Templated
               .m_axi_wstrb             (dma_axi_wstrb[MSTRBW-1:0]), // Templated
               .m_axi_wlast             (dma_axi_wlast),         // Templated
               .m_axi_wvalid            (dma_axi_wvalid),        // Templated
               .m_axi_bready            (dma_axi_bready),        // Templated
               .m_axi_arid              (dma_axi_arid[M_IDW-1:0]), // Templated
               .m_axi_araddr            (dma_axi_araddr[AW-1:0]), // Templated
               .m_axi_arlen             (dma_axi_arlen[7:0]),    // Templated
               .m_axi_arsize            (dma_axi_arsize[2:0]),   // Templated
               .m_axi_arburst           (dma_axi_arburst[1:0]),  // Templated
               .m_axi_arlock            (dma_axi_arlock),        // Templated
               .m_axi_arcache           (dma_axi_arcache[3:0]),  // Templated
               .m_axi_arprot            (dma_axi_arprot[2:0]),   // Templated
               .m_axi_arqos             (dma_axi_arqos[3:0]),    // Templated
               .m_axi_arvalid           (dma_axi_arvalid),       // Templated
               .m_axi_rready            (dma_axi_rready),        // Templated
               .s_axi_arregion          (m_axi_arregion[3:0]),   // Templated
               .s_axi_arready           (m_axi_arready),         // Templated
               .s_axi_awready           (m_axi_awready),         // Templated
               .s_axi_bid               (m_axi_bid[S_IDW-1:0]),  // Templated
               .s_axi_bresp             (m_axi_bresp[1:0]),      // Templated
               .s_axi_bvalid            (m_axi_bvalid),          // Templated
               .s_axi_rid               (m_axi_rid[S_IDW-1:0]),  // Templated
               .s_axi_rdata             (m_axi_rdata[DW-1:0]),   // Templated
               .s_axi_rlast             (m_axi_rlast),           // Templated
               .s_axi_rresp             (m_axi_rresp[1:0]),      // Templated
               .s_axi_rvalid            (m_axi_rvalid),          // Templated
               .s_axi_wready            (m_axi_wready),          // Templated
               .constant_zero           (constant_zero),
               .constant_one            (constant_one),
               // Inputs
               .m_axi_awready           (dma_axi_awready),       // Templated
               .m_axi_wready            (dma_axi_wready),        // Templated
               .m_axi_bid               (dma_axi_bid[M_IDW-1:0]), // Templated
               .m_axi_bresp             (dma_axi_bresp[1:0]),    // Templated
               .m_axi_bvalid            (dma_axi_bvalid),        // Templated
               .m_axi_arregion          (dma_axi_arregion[3:0]), // Templated
               .m_axi_arready           (dma_axi_arready),       // Templated
               .m_axi_rid               (dma_axi_rid[M_IDW-1:0]), // Templated
               .m_axi_rdata             (dma_axi_rdata[MDW-1:0]), // Templated
               .m_axi_rresp             (dma_axi_rresp[1:0]),    // Templated
               .m_axi_rlast             (dma_axi_rlast),         // Templated
               .m_axi_rvalid            (dma_axi_rvalid),        // Templated
               .nreset                  (nreset),
               .s_axi_arid              (m_axi_arid[S_IDW-1:0]), // Templated
               .s_axi_araddr            (m_axi_araddr[31:0]),    // Templated
               .s_axi_arburst           (m_axi_arburst[1:0]),    // Templated
               .s_axi_arcache           (m_axi_arcache[3:0]),    // Templated
               .s_axi_arlock            (m_axi_arlock),          // Templated
               .s_axi_arlen             (m_axi_arlen[7:0]),      // Templated
               .s_axi_arprot            (m_axi_arprot[2:0]),     // Templated
               .s_axi_arqos             (m_axi_arqos[3:0]),      // Templated
               .s_axi_arsize            (m_axi_arsize[2:0]),     // Templated
               .s_axi_arvalid           (m_axi_arvalid),         // Templated
               .s_axi_awid              (m_axi_awid[S_IDW-1:0]), // Templated
               .s_axi_awaddr            (m_axi_awaddr[31:0]),    // Templated
               .s_axi_awburst           (m_axi_awburst[1:0]),    // Templated
               .s_axi_awcache           (m_axi_awcache[3:0]),    // Templated
               .s_axi_awlock            (m_axi_awlock),          // Templated
               .s_axi_awlen             (m_axi_awlen[7:0]),      // Templated
               .s_axi_awprot            (m_axi_awprot[2:0]),     // Templated
               .s_axi_awqos             (m_axi_awqos[3:0]),      // Templated
               .s_axi_awregion          (m_axi_awregion[3:0]),   // Templated
               .s_axi_awsize            (m_axi_awsize[2:0]),     // Templated
               .s_axi_awvalid           (m_axi_awvalid),         // Templated
               .s_axi_bready            (m_axi_bready),          // Templated
               .s_axi_rready            (m_axi_rready),          // Templated
               .s_axi_wid               (m_axi_wid[S_IDW-1:0]),  // Templated
               .s_axi_wdata             (m_axi_wdata[DW-1:0]),   // Templated
               .s_axi_wlast             (m_axi_wlast),           // Templated
               .s_axi_wstrb             (m_axi_wstrb[3:0]),      // Templated
               .s_axi_wvalid            (m_axi_wvalid));         // Templated

   /*esaxi AUTO_TEMPLATE (//Stimulus
    .s_\(.*\)     (dma_\1[]),
    .wr_\(.*\)     (mem_txwr_\1[]),
    .rd_\(.*\)     (mem_rd_\1[]),
    .rr_\(.*\)     (mem_rr_\1[]),
    );
    */
   esaxi #(.DW(MDW),
           .S_IDW(M_IDW))
   esaxi (// Inputs
          .s_axi_aclk                   (clk1),
          .s_axi_aresetn                (nreset),

          .wr_wait                      (1'b0),
          .rd_wait                      (1'b0),
          .rr_access                    (1'b0),
          .rr_packet                    ({(PW){1'b0}}),

          .s_axi_wdata                  (dma_axi_wdata[MDW-1:0]),
          .s_axi_wstrb                  (dma_axi_wstrb[MSTRBW-1:0]),

          // Outputs
          .wr_access                    (),
          .wr_packet                    (),
          .rd_access                    (),
          .rd_packet                    (),
          .rr_wait                      (),

          .s_axi_rdata                  (dma_axi_rdata[MDW-1:0]),

          /*AUTOINST*/
          // Outputs
          .s_axi_arregion               (dma_axi_arregion[3:0]), // Templated
          .s_axi_arready                (dma_axi_arready),       // Templated
          .s_axi_awready                (dma_axi_awready),       // Templated
          .s_axi_bid                    (dma_axi_bid[S_IDW-1:0]), // Templated
          .s_axi_bresp                  (dma_axi_bresp[1:0]),    // Templated
          .s_axi_bvalid                 (dma_axi_bvalid),        // Templated
          .s_axi_rid                    (dma_axi_rid[S_IDW-1:0]), // Templated
          .s_axi_rlast                  (dma_axi_rlast),         // Templated
          .s_axi_rresp                  (dma_axi_rresp[1:0]),    // Templated
          .s_axi_rvalid                 (dma_axi_rvalid),        // Templated
          .s_axi_wready                 (dma_axi_wready),        // Templated
          // Inputs
          .s_axi_arid                   (dma_axi_arid[S_IDW-1:0]), // Templated
          .s_axi_araddr                 (dma_axi_araddr[AW-1:0]), // Templated
          .s_axi_arburst                (dma_axi_arburst[1:0]),  // Templated
          .s_axi_arcache                (dma_axi_arcache[3:0]),  // Templated
          .s_axi_arlock                 (dma_axi_arlock),        // Templated
          .s_axi_arlen                  (dma_axi_arlen[7:0]),    // Templated
          .s_axi_arprot                 (dma_axi_arprot[2:0]),   // Templated
          .s_axi_arqos                  (dma_axi_arqos[3:0]),    // Templated
          .s_axi_arsize                 (dma_axi_arsize[2:0]),   // Templated
          .s_axi_arvalid                (dma_axi_arvalid),       // Templated
          .s_axi_awid                   (dma_axi_awid[S_IDW-1:0]), // Templated
          .s_axi_awaddr                 (dma_axi_awaddr[AW-1:0]), // Templated
          .s_axi_awburst                (dma_axi_awburst[1:0]),  // Templated
          .s_axi_awcache                (dma_axi_awcache[3:0]),  // Templated
          .s_axi_awlock                 (dma_axi_awlock),        // Templated
          .s_axi_awlen                  (dma_axi_awlen[7:0]),    // Templated
          .s_axi_awprot                 (dma_axi_awprot[2:0]),   // Templated
          .s_axi_awqos                  (dma_axi_awqos[3:0]),    // Templated
          .s_axi_awregion               (dma_axi_awregion[3:0]), // Templated
          .s_axi_awsize                 (dma_axi_awsize[2:0]),   // Templated
          .s_axi_awvalid                (dma_axi_awvalid),       // Templated
          .s_axi_bready                 (dma_axi_bready),        // Templated
          .s_axi_rready                 (dma_axi_rready),        // Templated
          .s_axi_wid                    (dma_axi_wid[S_IDW-1:0]), // Templated
          .s_axi_wlast                  (dma_axi_wlast),         // Templated
          .s_axi_wvalid                 (dma_axi_wvalid));       // Templated

   endmodule // dv_axi
// Local Variables:
// verilog-library-directories:("." "../hdl" "../../emesh/dv" "../../axi/dv" "../../emesh/hdl" "../../memory/hdl" "../../axi/hdl")
// End:
