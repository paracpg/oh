module dut(/*AUTOARG*/
   // Outputs
   dut_active, clkout, wait_out, access_out, packet_out,
   // Inputs
   clk1, clk2, nreset, vdd, vss, access_in, packet_in, wait_in
   );

   parameter AW    = 32;
   parameter DW    = 32;
   parameter IDW   = 12;
   parameter M_IDW = 6;
   parameter S_IDW = 12;
   parameter PW    = 104;
   parameter N     = 1;
   parameter ID    = 12'h810;
   parameter WAIT  = 1;      // enable random wait

   //#######################################
   //# CLOCK AND RESET
   //#######################################
   input            clk1;
   input            clk2;
   input            nreset;
   input [N*N-1:0]  vdd;
   input 	    vss;
   output 	    dut_active;
   output 	    clkout;

   //#######################################
   //#EMESH INTERFACE
   //#######################################

   //Stimulus Driven Transaction
   input [N-1:0]     access_in;
   input [N*PW-1:0]  packet_in;
   output [N-1:0]    wait_out;

   //DUT driven transactoin
   output [N-1:0]    access_out;
   output [N*PW-1:0] packet_out;
   input [N-1:0]     wait_in;

   //TODO: finish readback

   wire 	    wait_random;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [4:0]           ctrlmode_in;            // From e2p of packet2emesh.v
   wire [AW-1:0]        data_in;                // From e2p of packet2emesh.v
   wire [1:0]           datamode_in;            // From e2p of packet2emesh.v
   wire [AW-1:0]        dstaddr_in;             // From e2p of packet2emesh.v
   wire [AW-1:0]        srcaddr_in;             // From e2p of packet2emesh.v
   wire                 write_in;               // From e2p of packet2emesh.v
   // End of automatics

   assign clkout     = clk1;
   assign dut_active         = 1'b1;
   assign wait_out           = 'b0;

   /*packet2emesh _AUTO_TEMPLATE_ (//Stimulus
                            .\(.*\)_out(mi_\1[]),
                             );
   */

   //CONFIG INTERFACE
   packet2emesh e2p (// Outputs
                     .datamode_in       (datamode_in[1:0]),
                     .ctrlmode_in       (ctrlmode_in[4:0]),
                     .dstaddr_in        (dstaddr_in[AW-1:0]),
                     .srcaddr_in        (srcaddr_in[AW-1:0]),
                     .data_in           (data_in[AW-1:0]),
                     // Inputs
                     .packet_in         (packet_in[PW-1:0]),
                     /*AUTOINST*/
                     // Outputs
                     .write_in          (write_in));

   //TRACE
   etrace #(.ID(ID),
            .DW(32),
            .VW(32))
   etrace ( // Outputs
            .data_txwr_access (),
            .data_txwr_packet (),
            .s_access_out (access_out),
            .s_packet_out (packet_out),
            // Inputs
            .data_txwr_wait              (wait_random),
            .trace_clk (clk1),
            .trace_trigger (1'b1),
            .trace_vector (srcaddr_in[AW-1:0]),
            .clk (clk1),
            .s_access_in (access_in),
            .s_packet_in (packet_in[PW-1:0]),
           /*AUTOINST*/
           // Inputs
           .nreset                      (nreset));

   //Random wait generator
   generate
      if(WAIT)
	begin
	   reg [8:0] wait_counter;
	   always @ (posedge clk1 or negedge nreset)
	     if(!nreset)
	       wait_counter[8:0] <= 'b0;
	     else
	       wait_counter[8:0] <= wait_counter+1'b1;
	   assign wait_random      = (|wait_counter[3:0]);//1'b0;
	end
      else
	begin
	   assign wait_random = 1'b0;
	end // else: !if(WAIT)
   endgenerate

endmodule // dv_elink
// Local Variables:
// verilog-library-directories:("." "../hdl" "../../emesh/dv" "../../emesh/hdl")
// End:
