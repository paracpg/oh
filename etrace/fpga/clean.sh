#!/bin/sh

COMPONENT=axi_etrace
REALLY_CLEAN=0
if [ $REALLY_CLEAN -eq 1 ]
then
    rm -rf $COMPONENT
fi

rm -rf $COMPONENT.bit.bin
rm -rf $COMPONENT.cache
rm -rf $COMPONENT.hw
rm -rf $COMPONENT.ip_user_files
rm -rf $COMPONENT.runs
rm -rf $COMPONENT.sim
rm -rf $COMPONENT.xpr
rm -rf $COMPONENT.zip
rm -rf component.xml
rm -rf ip_tmp
rm -rf NA
rm -rf reports
rm -rf results
rm -rf src
rm -rf system.cache
rm -rf system.gen
rm -rf system.hw
rm -rf system.ip_user_files
rm -rf system.runs
rm -rf system.sim
rm -rf system.srcs
rm -rf system_wrapper.bit.bin
rm -rf system.xpr
rm -rf vivado*.jou
rm -rf vivado*.log
rm -rf xgui
rm -rf .Xil
