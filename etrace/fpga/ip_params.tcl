# NOTE: See UG1118 for more information

set design axi_etrace
set projdir ./
set root "../.."

set hdl_files [list \
                   $root/axi/hdl \
                   $root/stdlib/rtl \
                   $root/emesh/hdl \
                   $root/etrace/hdl \
                  ]

set ip_files [list \
                  $root/xilibs/ip_zynq/fifo_async_104x32.xci \
                 ]

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
