#!/bin/bash

##############################
#Create directory of all links
##############################
if [ -d "symlinks" ]
then
    rm -r $OH_HOME/symlinks
fi
mkdir -p $OH_HOME/symlinks/hdl
mkdir -p $OH_HOME/symlinks/dv
pushd $OH_HOME/symlinks/hdl > /dev/null
ln -s ../../*/hdl/*.{v,vh} .
ln -s ../../*/rtl/*.{v,vh} .
cd ../dv
ln -s ../../*/dv/*.v .
ln -s ../../stdlib/testbench/*.v .
popd > /dev/null
