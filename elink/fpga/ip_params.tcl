# NOTE: See UG1118 for more information

set design axi_elink
set projdir ./
set root "../.."

set hdl_files [list \
	           $root/gpio/hdl \
		   $root/stdlib/rtl/ \
		   $root/emesh/hdl \
		   $root/emmu/hdl \
		   $root/axi/hdl \
		   $root/emailbox/hdl \
		   $root/edma/hdl \
	           $root/elink/hdl \
	           $root/parallella/hdl \
		  ]

set ip_repos [list \
                  $root/parallella/ip_repo \
                  $root/parallella/fpga/parallella_base \
                 ]

set ip_files   [list \
                    $root/xilibs/ip_zynq/fifo_async_104x32.xci \
		         ]

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
set partname "xc7z020clg400-1"
