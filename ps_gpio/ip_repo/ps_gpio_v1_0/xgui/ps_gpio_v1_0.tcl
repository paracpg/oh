# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "DIFF" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NGPIO" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NPS" -parent ${Page_0}


}

proc update_PARAM_VALUE.DIFF { PARAM_VALUE.DIFF } {
	# Procedure called to update DIFF when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DIFF { PARAM_VALUE.DIFF } {
	# Procedure called to validate DIFF
	return true
}

proc update_PARAM_VALUE.NGPIO { PARAM_VALUE.NGPIO } {
	# Procedure called to update NGPIO when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NGPIO { PARAM_VALUE.NGPIO } {
	# Procedure called to validate NGPIO
	return true
}

proc update_PARAM_VALUE.NPS { PARAM_VALUE.NPS } {
	# Procedure called to update NPS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NPS { PARAM_VALUE.NPS } {
	# Procedure called to validate NPS
	return true
}


proc update_MODELPARAM_VALUE.NGPIO { MODELPARAM_VALUE.NGPIO PARAM_VALUE.NGPIO } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NGPIO}] ${MODELPARAM_VALUE.NGPIO}
}

proc update_MODELPARAM_VALUE.NPS { MODELPARAM_VALUE.NPS PARAM_VALUE.NPS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NPS}] ${MODELPARAM_VALUE.NPS}
}

proc update_MODELPARAM_VALUE.DIFF { MODELPARAM_VALUE.DIFF PARAM_VALUE.DIFF } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DIFF}] ${MODELPARAM_VALUE.DIFF}
}

