
module ps_gpio (/*AUTOARG*/
   // Outputs
   ps_gpio_i, constant_zero, constant_one,
   // Inouts
   gpio_p, gpio_n,
   // Inputs
   ps_gpio_t, ps_gpio_o
   );

   parameter NGPIO = 24;   // 12 or 24
   parameter NPS   = 64;   // Number of signals for PS
   parameter DIFF  = 0;    // 0= single ended
                           // 1= differential

   // constants
   output		constant_zero;		// Always 0
   output		constant_one;		// Always 1

   /*AUTOINPUT*/
   // Beginning of automatic inputs (from unused autoinst inputs)
   input [NPS-1:0]      ps_gpio_o;              // To pgpio_inst of pgpio.v
   input [NPS-1:0]      ps_gpio_t;              // To pgpio_inst of pgpio.v
   // End of automatics

   /*AUTOINOUT*/
   // Beginning of automatic inouts (from unused autoinst inouts)
   inout [NGPIO-1:0]    gpio_n;                 // To/From pgpio_inst of pgpio.v
   inout [NGPIO-1:0]    gpio_p;                 // To/From pgpio_inst of pgpio.v
   // End of automatics

   /*AUTOOUTPUT*/
   // Beginning of automatic outputs (from unused autoinst outputs)
   output [NPS-1:0]     ps_gpio_i;              // From pgpio_inst of pgpio.v
   // End of automatics

   assign constant_zero = 1'b0;
   assign constant_one = 1'b1;

   //################################
   //# call GPIO module
   //################################
   pgpio
     #(
       .NGPIO (NGPIO),
       .DIFF  (DIFF),
       .NPS   (NPS))
   pgpio_inst
     (
      /*AUTOINST*/
      // Outputs
      .ps_gpio_i                        (ps_gpio_i[NPS-1:0]),
      // Inouts
      .gpio_p                           (gpio_p[NGPIO-1:0]),
      .gpio_n                           (gpio_n[NGPIO-1:0]),
      // Inputs
      .ps_gpio_o                        (ps_gpio_o[NPS-1:0]),
      .ps_gpio_t                        (ps_gpio_t[NPS-1:0]));

endmodule // ps_gpio

// Local Variables:
// verilog-library-directories:("." "../../*/hdl")
// End:
