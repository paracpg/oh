
#STEP1: DEFINE KEY PARAMETERS
source ./system_params.tcl

#STEP2: CREATE PROJECT AND READ IN FILES
source ../../../stdlib/fpga/system_init.tcl

#STEP 3 (OPTIONAL): EDIT system.bd in VIVADO gui, then go to STEP 4.
##...

#STEP 4: SYNTEHSIZE AND CREATE BITSTRAM
source ../../../stdlib/fpga/system_build.tcl
