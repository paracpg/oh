# NOTE: See UG1118 for more information

#########################################
# VARIABLES
#########################################
set design parallella_base
set projdir ./
set root "../../.."

set hdl_files [list \
	           $root/parallella/hdl \
		   $root/stdlib/rtl/ \
		   $root/emesh/hdl \
		   $root/emmu/hdl \
		   $root/axi/hdl \
		   $root/emailbox/hdl \
		   $root/edma/hdl \
	           $root/elink/hdl \
		  ]

set ip_repos [list \
                  "$root/parallella/ip_repo" \
                  "$root/elink/ip_repo" \
                 ]

set ip_files [list \
		          $root/xilibs/ip_zynq/fifo_async_104x32.xci \
		         ]

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
