###########################################################
# PREPARE FOR SYNTHESIS
###########################################################
set partname "xc7z010clg400-1"
set oh_verilog_define "CFG_ASIC=0"
set oh_synthesis_options "-verilog_define ${oh_verilog_define}"
