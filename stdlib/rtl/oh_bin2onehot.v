//#############################################################################
//# Function: Binary to one hot encoder                                       #
//#############################################################################
//# Author:   Andreas Olofsson                                                #
//# License:  MIT (see LICENSE file in OH! repository)                        # 
//#############################################################################

module oh_bin2onehot #(parameter DW = 32, // width of data inputs
                       parameter NB = 5)  // encoded bit width
(
 input [NB-1:0]  in, // unsigned binary input  
 output [DW-1:0] out   // one hot output vector
 );

   integer 	  i;      
   reg [DW-1:0] 	  reg_out;  

   always @*
     for(i=0;i<DW;i=i+1)
       reg_out[i]=(in[NB-1:0]==i);

   assign out = reg_out;
   
endmodule // oh_bin2onehot





