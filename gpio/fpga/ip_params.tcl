# NOTE: See UG1118 for more information

set design parallella_gpio
set projdir ./
set root "../.."

set hdl_files [list \
	           $root/gpio/hdl \
		   $root/stdlib/rtl/ \
		   $root/emesh/hdl \
		   $root/emmu/hdl \
		   $root/axi/hdl \
		   $root/emailbox/hdl \
		   $root/edma/hdl \
	           $root/elink/hdl \
	           $root/parallella/hdl \
		  ]

set ip_files   []

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
