# NOTE: See UG1118 for more information

set design axi_accelerator
set projdir ./
set root "../.."

set hdl_files [list \
	           $root/accelerator/hdl \
		   $root/stdlib/rtl/ \
		   $root/emesh/hdl \
		   $root/emmu/hdl \
		   $root/axi/hdl \
		   $root/emailbox/hdl \
		   $root/edma/hdl \
	           $root/elink/hdl \
		  ]

set ip_files   []

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
set partname "xc7z020clg400-1"
