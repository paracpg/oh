#!/bin/sh

if [ $_ = $0 ]
then
  echo "ERROR: to set the environment variables source this script"
  echo "  source setupenv.sh"
  exit;
fi

SRCPATH=$(python3 -c "import os; print(os.path.dirname(os.path.realpath('${BASH_SOURCE[0]}')))")
#Setting OH_HOME variable
export OH_HOME=$SRCPATH

if [[ -e "$SRCPATH/../setupenv.sh" ]] ; then
    # optional script to setup vivado / vitis
    . $SRCPATH/../setupenv.sh
fi
