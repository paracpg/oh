# NOTE: See UG1118 for more information

# set design axi_lite_monitor
set design axi_monitor
set projdir ./
set root "../.."

set hdl_files [list \
                   $root/axi/hdl \
		  ]

set ip_repos []

set ip_files   []

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
