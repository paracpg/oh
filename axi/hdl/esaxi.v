module esaxi (/*autoarg*/
   // Outputs
   wr_access, wr_packet, rd_access, rd_packet, rr_wait,
   s_axi_arregion, s_axi_arready, s_axi_bid, s_axi_bresp,
   s_axi_bvalid, s_axi_rid, s_axi_rdata, s_axi_rlast, s_axi_rresp,
   s_axi_rvalid, s_axi_wready,
   // Inputs
   wr_wait, rd_wait, rr_access, rr_packet, s_axi_aclk, s_axi_aresetn,
   s_axi_arid, s_axi_araddr, s_axi_arburst, s_axi_arcache,
   s_axi_arlock, s_axi_arlen, s_axi_arprot, s_axi_arqos, s_axi_arsize,
   s_axi_arvalid, s_axi_awid, s_axi_awaddr, s_axi_awburst,
   s_axi_awcache, s_axi_awlock, s_axi_awlen, s_axi_awprot,
   s_axi_awqos, s_axi_awregion, s_axi_awsize, s_axi_awvalid,
   s_axi_awready, s_axi_bready, s_axi_rready, s_axi_wid, s_axi_wdata,
   s_axi_wlast, s_axi_wstrb, s_axi_wvalid
   );

   parameter          S_IDW               = 12;
   parameter          PW                  = 104;
   parameter          AW                  = 32;
   parameter [AW-1:0] RETURN_ADDR         = 0;
   parameter          DW                  = 32;
   parameter          LW                  = 64; // size of packet data
   localparam         WW                  = 32; // size of rr_data
   localparam         STRBW               = DW / 8;

`ifdef TARGET_SIM
   parameter         TW                  = 16;  //timeout counter width
`else
   parameter         TW                  = 16;  //timeout counter width
`endif
 
   //#############################
   //# Write request
   //#############################
   output          wr_access;
   output [PW-1:0] wr_packet;
   input           wr_wait;

   //#############################
   //# Read request
   //#############################
   output          rd_access;
   output [PW-1:0] rd_packet;
   input           rd_wait;

   //#############################
   //# Read response
   //#############################
   input           rr_access;
   input [PW-1:0]  rr_packet;
   output          rr_wait;

   //#############################
   //# AXI Slave Interface
   //#############################

   //Clock and reset
   input       s_axi_aclk;
   input       s_axi_aresetn;

   //Read address channel
   input [S_IDW-1:0] s_axi_arid;    //write address ID
   input [AW-1:0]      s_axi_araddr;
   input [1:0]       s_axi_arburst;
   input [3:0]       s_axi_arcache;
   input             s_axi_arlock;
   input [7:0]       s_axi_arlen;
   input [2:0]       s_axi_arprot;
   input [3:0]       s_axi_arqos; // AXI4
   output [3:0]      s_axi_arregion; // AXI4
   output            s_axi_arready;
   input [2:0]       s_axi_arsize;
   input             s_axi_arvalid;

   //Write address channel
   input [S_IDW-1:0] s_axi_awid;    //write address ID
   input [AW-1:0]      s_axi_awaddr;
   input [1:0]       s_axi_awburst;
   input [3:0]       s_axi_awcache;
   input             s_axi_awlock;
   input [7:0]       s_axi_awlen;
   input [2:0]       s_axi_awprot;
   input [3:0]       s_axi_awqos; // AXI4
   input [3:0]       s_axi_awregion; // AXI4

   input [2:0]       s_axi_awsize;
   input             s_axi_awvalid;
   output            s_axi_awready;
   
   //Buffered write response channel
   output [S_IDW-1:0] s_axi_bid;    //write address ID
   output [1:0]       s_axi_bresp;
   output             s_axi_bvalid;
   input              s_axi_bready;

   //Read channel
   output [S_IDW-1:0] s_axi_rid;    //write address ID
   output [DW-1:0]      s_axi_rdata;
   output             s_axi_rlast;
   output [1:0]       s_axi_rresp;
   output             s_axi_rvalid;
   input              s_axi_rready;

   //Write channel
   input [S_IDW-1:0]  s_axi_wid;    //write address ID
   input [DW-1:0]     s_axi_wdata;
   input              s_axi_wlast;
   input [DW/8-1:0]        s_axi_wstrb;
   input              s_axi_wvalid;
   output             s_axi_wready;

   //###################################################
   //#WIRE/REG DECLARATIONS
   //###################################################

   reg               s_axi_awready;
   reg               s_axi_wready;
   reg               s_axi_bvalid;
   reg [1:0]         s_axi_bresp;

   reg [AW-1:0]      awaddr_b;    // 32b for epiphany addr
   reg [1:0]         awburst_b;
   reg [2:0]         awsize_b;
   reg [S_IDW-1:0]   bid_b;       //what to do with this?

   reg [AW-1:0]      araddr_b;
   reg [7:0]         arlen_b;
   reg [1:0]         arburst_b;
   reg [2:0]         arsize_b;

   reg [DW-1:0]      s_axi_rdata;
   reg [1:0]         s_axi_rresp;
   reg               s_axi_rlast;
   reg [S_IDW-1:0]   s_axi_rid;
   reg               rvalid_b;     // delay rvalid after wait
   reg               arready_b;    // delay arready after wait

   reg               read_active_start;
   reg               read_active_b; // need leading edge of active for 1st req
   reg               read_end_b;    // need trailing edge for next req

   reg [AW-1:0]      read_addr;
   reg               write_active;
   reg               write_response_wait_b; // waiting to issue write response (unlikely?)

   reg               wr_access;
   reg [1:0]         wr_datamode;
   reg [AW-1:0]      wr_dstaddr;
   reg [WW-1:0]      wr_data;    // data_lo
   reg [AW-1:0]      wr_srcaddr; // data_hi

   reg [WW-1:0]      wr_data_reg;
   reg [AW-1:0]      wr_srcaddr_reg;
   reg [AW-1:0]      wr_dstaddr_reg;
   reg [1:0]         wr_datamode_reg;

   reg               rd_access;
   reg [1:0]         rd_datamode;
   reg [AW-1:0]      rd_dstaddr;
   reg [AW-1:0]      rd_srcaddr;   // read response address

   reg               pre_wr_en;    // delay for data alignment

   reg               rnext;

   wire              s_axi_rvalid;
   wire              s_axi_arready;
   wire              read_active;

   wire              last_wr_beat;
   wire              last_rd_beat;

   wire [AW-1:0]     rr_srcaddr;
   wire [WW-1:0]     rr_data;
   wire              rr_return_access;
   wire [LW-1:0]     rr_return_data;
   reg [LW-1:0]      rr_return_data_fifo;
   reg [TW-1:0]      timeout_counter;
   wire              rr_timeout_access;
   wire              counter_expired;

   //###################################################
   //#PACKET TO MESH
   //###################################################

   //WR
   emesh2packet e2p_wr (
             // Outputs
             .packet_out     (wr_packet[PW-1:0]),
             // Inputs
             .write_out      (1'b1),
             .datamode_out   (wr_datamode[1:0]),
             .ctrlmode_out   (5'b0),
             .dstaddr_out    (wr_dstaddr[AW-1:0]),
             .data_out       (wr_data[WW-1:0]),
             .srcaddr_out    (wr_srcaddr[AW-1:0])
             );

   //RD
   emesh2packet e2p_rd (
             // Outputs
             .packet_out     (rd_packet[PW-1:0]),
             // Inputs
             .write_out      (1'b0),
             .datamode_out   (rd_datamode[1:0]),
             .ctrlmode_out   (5'b0),
             .dstaddr_out    (rd_dstaddr[AW-1:0]),
             .data_out       ({(WW){1'b0}}),
             .srcaddr_out    (rd_srcaddr[AW-1:0])
             );
   //RR
   packet2emesh p2e_rr (
              // Outputs
              .write_in      (),
              .datamode_in   (),
              .ctrlmode_in   (),
              .dstaddr_in    (),
              .data_in       (rr_data[WW-1:0]),
              .srcaddr_in    (rr_srcaddr[AW-1:0]),
              // Inputs
              .packet_in     (rr_packet[PW-1:0])
              );

   //#########################################################################
   //AXI unimplemented constants
   //#########################################################################

   // AR
   assign s_axi_arregion[3:0]	= 4'b0000; // AXI4

   //###################################################
   //#WRITE ADDRESS CHANNEL
   //###################################################

   assign  last_wr_beat = s_axi_wready & s_axi_wvalid & s_axi_wlast;

   // axi_awready is asserted when there is no write transfer in progress
   always @(posedge s_axi_aclk ) 
     begin
        if(~s_axi_aresetn)
          begin
             s_axi_awready <= 1'b1;
             write_active  <= 1'b0;
          end
        else
          begin
             // we're always ready for an address cycle if we're not doing something else
             // note: might make this faster by going ready on last beat instead of after,
             // but if we want the very best each channel should be fifo'd.
             if( ~s_axi_awready & ~write_active & ~write_response_wait_b )
               s_axi_awready <= 1'b1;
             else if( s_axi_awvalid )
               s_axi_awready <= 1'b0;

             // the write cycle is "active" as soon as we capture an address, it
             // ends on the last beat.
             if( s_axi_awready & s_axi_awvalid )
               write_active <= 1'b1;
             else if( last_wr_beat )
               write_active <= 1'b0;
          end // else: !if(~s_axi_aresetn)
     end // always @ (posedge s_axi_aclk )

     always @( posedge s_axi_aclk ) 
     if (~s_axi_aresetn)  
       begin
          bid_b[S_IDW-1:0] <= 'd0;  // capture for write response
          awaddr_b[AW-1:0] <= {(AW){1'b0}};
          awsize_b[2:0]    <= 3'd0;
          awburst_b[1:0]   <= 2'd0;
       end 
     else
       begin
          if( s_axi_awready & s_axi_awvalid )
            begin
               bid_b[S_IDW-1:0] <= s_axi_awid[S_IDW-1:0];
               awaddr_b[AW-1:0] <= s_axi_awaddr[AW-1:0];
               awsize_b[2:0]    <= s_axi_awsize[2:0];  // 0=byte, 1=16b, 2=32b
               awburst_b[1:0]   <= s_axi_awburst[1:0]; // type, 0=fixed, 1=incr, 2=wrap
            end
          else if( s_axi_wvalid & s_axi_wready )
            if( awburst_b == 2'b01 ) 
              begin //incremental burst
                 awaddr_b[AW-1:2] <= awaddr_b[AW-1:2] + 29'd1;  // 1 in AW-3 bit number
                 awaddr_b[1:0]  <= 2'b0;
              end
       end // else: !if(~s_axi_aresetn)

   //###################################################
   //#WRITE RESPONSE CHANNEL
   //###################################################
   assign s_axi_bid = bid_b;

   always @ (posedge s_axi_aclk)
     if(~s_axi_aresetn) 
       s_axi_wready <= 1'b0;      
     else
       begin
          if( last_wr_beat )
            s_axi_wready <= 1'b0;
          else if( write_active )
            s_axi_wready <= ~wr_wait;
       end                             

   always @( posedge s_axi_aclk )
     if (~s_axi_aresetn) 
       begin
          s_axi_bvalid          <= 1'b0;
          s_axi_bresp[1:0]      <= 2'b0;
          write_response_wait_b <= 1'b0;
       end 
     else 
       begin         
          if( last_wr_beat )
            begin
               s_axi_bvalid          <= 1'b1;
               s_axi_bresp[1:0]      <= 2'b0;           // 'okay' response
               write_response_wait_b <= ~s_axi_bready;  // note: assumes bready will not drop without valid?
            end
          else if (s_axi_bready & s_axi_bvalid)
            begin
               s_axi_bvalid          <= 1'b0;
               write_response_wait_b <= 1'b0;
            end
       end // else: !if( s_axi_aresetn == 1'b0 )

   //###################################################
   //#READ REQUEST CHANNEL
   //###################################################  

   // TODO s_axi_rid, 
   // TODO s_axi_bid, 
   // TODO s_axi_rvalid, 
   // TODO s_axi_arready,
   // TODO s_axi_rdata, 
   // TODO s_axi_rresp, 
   // TODO s_axi_awready, 
   // TODO s_axi_bresp, 
   // TODO s_axi_bvalid, 
   // TODO s_axi_wready
   assign  last_rd_beat = s_axi_rvalid & s_axi_rlast & s_axi_rready;
   assign  rd_wait_in = rd_wait & ~read_active;
   assign  s_axi_arready = arready_b & ~rd_wait_in;
   assign  read_active = read_active_start & ~read_end_b;

   always @( posedge s_axi_aclk )
     if (~s_axi_aresetn)
       begin
          arready_b         <= 1'b1;
          read_active_start <= 1'b0;
          read_end_b        <= 1'b0;
       end 
     else 
       begin
          //arready
          if( ~arready_b & ( last_rd_beat | ~read_active ) & ~read_end_b )
            arready_b <= 1'b1;
          else if( s_axi_arvalid & ~rd_wait )
            arready_b <= 1'b0;

          //read_active
          if( s_axi_arready & s_axi_arvalid )
            read_active_start <= 1'b1;
          else if( last_rd_beat )
            read_active_start <= 1'b0;

          //read_end
          if( ~read_active & read_active_b )
            read_end_b <= 1'b1;
          else if( read_end_b )
            read_end_b <= 1'b0;

       end // else: !if( s_axi_aresetn == 1'b0 )

   //Read address channel state machine
   always @( posedge s_axi_aclk )
     if (~s_axi_aresetn)
       begin
           araddr_b[31:0]   <= 0;
           arlen_b[7:0]     <= 8'd0;
           arburst_b        <= 2'd0;
           arsize_b[2:0]    <= 3'b0;
           s_axi_rlast        <= 1'b0;
           s_axi_rid[S_IDW-1:0] <= 'd0;         
       end
     else
       begin
          if( s_axi_arvalid & s_axi_arready )
            begin
               araddr_b[31:0]   <= s_axi_araddr[31:0]; //NOTE: upper 2 bits get chopped by Zynq
               arlen_b[7:0]     <= s_axi_arlen[7:0];
               arburst_b        <= s_axi_arburst;
               arsize_b         <= s_axi_arsize;
               s_axi_rlast        <= ~(|s_axi_arlen[7:0]);
               s_axi_rid[S_IDW-1:0] <= s_axi_arid[S_IDW-1:0];
            end
          else if( s_axi_rvalid & s_axi_rready & ~last_rd_beat )
            begin
               arlen_b[7:0] <= arlen_b[7:0] - 1;
               if(arlen_b[7:0] == 8'd1)
                 s_axi_rlast <= 1'b1;
               if( s_axi_arburst == 2'b01 )
                 begin //incremental burst
                    araddr_b[31:2] <= araddr_b[31:2] + 1;
                    araddr_b[1:0]  <= 2'b0;
                 end
            end // if ( s_axi_rvalid & s_axi_rready)
       end // else: !if( s_axi_aresetn == 1'b0 )


   //###################################################
   //#WRITE REQUEST
   //###################################################

   generate
      if (DW == 32)
   always @( posedge s_axi_aclk )
     if (~s_axi_aresetn)
       begin
          wr_data_reg[WW-1:0]    <= {(WW){1'd0}};
          wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
          wr_dstaddr_reg[AW-1:0] <= {(AW){1'd0}};
          wr_datamode_reg[1:0]   <= 2'd0;
          wr_access              <= 1'b0;
          pre_wr_en              <= 1'b0;
       end 
     else 
       begin
          pre_wr_en               <= s_axi_wready & s_axi_wvalid;
          wr_access               <= pre_wr_en;
          wr_datamode_reg[1:0]    <= awsize_b[1:0];
          wr_dstaddr_reg[AW-1:2]    <= awaddr_b[AW-1:2]; //set lsbs of address based on write strobes
          case(awsize_b[1:0])
            2'd0: // byte
              begin
                 wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
              case(s_axi_wstrb[STRBW-1:0])
                8'h01:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[7:0]};
                     wr_dstaddr_reg[1:0] <= 2'd0;
                  end
                8'h02:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[15:8]};
                     wr_dstaddr_reg[1:0] <= 2'd1;
                  end
                8'h04:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[23:16]};
                     wr_dstaddr_reg[1:0] <= 2'd2;
                  end
                default: // 8'h08:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[31:24]};
                     wr_dstaddr_reg[1:0] <= 2'd3;
                  end
              endcase // case (s_axi_wstrb[3:0])
              end
            2'd1: // 16b hword
              begin
                 wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
              case(s_axi_wstrb[STRBW-1:0])
                8'h03:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-16){1'b0}}, s_axi_wdata[15:0]};
                     wr_dstaddr_reg[1:0] <= 2'd0;
                  end
                default: // 8'h0c:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-16){1'b0}}, s_axi_wdata[31:16]};
                     wr_dstaddr_reg[1:0] <= 2'd2;
                  end
              endcase // case (s_axi_wstrb[3:0])
              end
            default: // 2'd2:
              begin
                 wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
                 wr_data_reg[WW-1:0] <= s_axi_wdata[31:0];
                 wr_dstaddr_reg[1:0] <= 2'd0;
              end
          endcase // case (awsize_b[1:0])
       end // else: !if(~s_axi_aresetn)
      else // DW == 64
   always @( posedge s_axi_aclk )
     if (~s_axi_aresetn)
       begin
          wr_data_reg[WW-1:0]    <= {(WW){1'd0}};
          wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
          wr_dstaddr_reg[AW-1:0] <= {(AW){1'd0}};
          wr_datamode_reg[1:0]   <= 2'd0;
          wr_access              <= 1'b0;
          pre_wr_en              <= 1'b0;
       end
     else
       begin
          pre_wr_en               <= s_axi_wready & s_axi_wvalid;
          wr_access               <= pre_wr_en;
          wr_datamode_reg[1:0]    <= awsize_b[1:0];
          wr_dstaddr_reg[AW-1:2]    <= awaddr_b[AW-1:2]; //set lsbs of address based on write strobes
          case(awsize_b[1:0])
            2'd0: // byte
              begin
                 wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
              case(s_axi_wstrb[STRBW-1:0])
                8'h01:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[7:0]};
                     wr_dstaddr_reg[1:0] <= 2'd0;
                  end
                8'h02:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[15:8]};
                     wr_dstaddr_reg[1:0] <= 2'd1;
                  end
                8'h04:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[23:16]};
                     wr_dstaddr_reg[1:0] <= 2'd2;
                  end
                8'h08:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[31:24]};
                     wr_dstaddr_reg[1:0] <= 2'd3;
                  end
                8'h10:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[39:32]};
                     wr_dstaddr_reg[1:0] <= 2'd0;
                  end
                8'h20:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[47:40]};
                     wr_dstaddr_reg[1:0] <= 2'd1;
                  end
                8'h40:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[55:48]};
                     wr_dstaddr_reg[1:0] <= 2'd2;
                  end
                default: // 8'h80
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-8){1'b0}}, s_axi_wdata[63:56]};
                     wr_dstaddr_reg[1:0] <= 2'd3;
                  end
              endcase // case (s_axi_wstrb[7:0])
              end
            2'd1: // 16b hword
              begin
                 wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
              case(s_axi_wstrb[STRBW-1:0])
                8'h03:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-16){1'b0}}, s_axi_wdata[15:0]};
                     wr_dstaddr_reg[1:0] <= 2'd0;
                  end
                8'h0c:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-16){1'b0}}, s_axi_wdata[31:16]};
                     wr_dstaddr_reg[1:0] <= 2'd2;
                  end
                8'h30:
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-16){1'b0}}, s_axi_wdata[47:32]};
                     wr_dstaddr_reg[1:0] <= 2'd0;
                  end
                default: // 8'hc0
                  begin
                     wr_data_reg[WW-1:0]   <= {{(WW-16){1'b0}}, s_axi_wdata[63:48]};
                     wr_dstaddr_reg[1:0] <= 2'd2;
                  end
              endcase // case (s_axi_wstrb[7:0])
              end
            2'd2: // 32b word
              begin
                 wr_srcaddr_reg[AW-1:0] <= {(AW){1'd0}};
                 case(s_axi_wstrb[STRBW-1:0])
                   8'h0f:
                     begin
                        wr_data_reg[WW-1:0] <= s_axi_wdata[31:0];
                        wr_dstaddr_reg[1:0] <= 2'd0;
                     end
                   default: // 8'hf0
                     begin
                        wr_data_reg[WW-1:0]   <= s_axi_wdata[63:32];
                        wr_dstaddr_reg[1:0] <= 2'd0;
                     end
                 endcase
              end
            default: // 64b word
              begin
                 wr_data_reg[WW-1:0]   <= s_axi_wdata[31:0];
                 wr_srcaddr_reg[WW-1:0] <= s_axi_wdata[63:32];
                 wr_dstaddr_reg[1:0] <= 2'd0;
              end
               endcase // case (awsize_b[2:0])
       end // else: !if(~s_axi_aresetn)
   endgenerate

   //Pipeline stage!
   always @( posedge s_axi_aclk )     
     begin
        wr_data[WW-1:0]      <= wr_data_reg[WW-1:0];
        wr_srcaddr[AW-1:0] <= wr_srcaddr_reg[AW-1:0];
        wr_dstaddr[AW-1:0]   <= wr_dstaddr_reg[AW-1:0];
        wr_datamode[1:0]   <= wr_datamode_reg[1:0];
     end

   //###################################################
   //#READ REQUEST (DATA CHANNEL)
   //###################################################  
   // -- reads are performed by sending a read
   // -- request out the tx port and waiting for
   // -- data to come back through the rx read response port.
   // --
   // -- because elink reads are not generally 
   // -- returned in order, we will only allow
   // -- one at a time.

   always @( posedge s_axi_aclk )
     if (~s_axi_aresetn) 
       begin
          rd_access          <= 1'b0;
          rd_datamode[1:0]   <= 2'd0;
          rd_dstaddr[AW-1:0] <= {(AW){1'b0}};
          rd_srcaddr[AW-1:0] <= {(AW){1'b0}};
          read_active_b      <= 1'b0;
          rnext              <= 1'b0;
       end
     else
       if (!rd_wait) // TODO rework this - what if rd_wait is active?
         begin
            read_active_b       <= read_active;
            rnext             <= s_axi_rvalid & s_axi_rready & ~s_axi_rlast;
            rd_access         <= ( read_active & ~read_active_b ) | rnext;
            rd_datamode[1:0]  <= arsize_b[1:0];
            rd_dstaddr[AW-1:0]  <= araddr_b[AW-1:0];
            rd_srcaddr[AW-1:0]  <= RETURN_ADDR;
            //TODO: use arid+srcaddr for out of order ?
         end

   //###################################################
   //#READ RESPONSE (DATA CHANNEL)
   //###################################################  
   //Read response AXI state machine
   //Only one outstanding read

   assign rr_wait = 1'b0;
   assign s_axi_rvalid = rvalid_b & ~rd_wait;

   assign rr_return_access     = rr_access | rr_timeout_access;
   assign rr_return_data[LW-1:0] = rr_timeout_access ? {2{32'hDEADBEEF}} :  // LW/WW lots of WW bits
                                 {rr_srcaddr[WW-1:0], rr_data[WW-1:0]};  // would need extension if WW*2 < LW

   generate
      if (DW == 32)
   always @( posedge s_axi_aclk )
     if (!s_axi_aresetn) 
       begin
          rvalid_b        <= 1'b0;
          s_axi_rdata[DW-1:0] <= {(DW){1'b0}};
          s_axi_rresp         <= 2'd0;
          rr_return_data_fifo[LW-1:0] <= {(LW){1'b0}};
       end
     else
       begin
          if( rr_return_access )
            begin
               rvalid_b <= 1'b1;
               s_axi_rresp  <= rr_timeout_access ? 2'b10 : 2'b00;
               rr_return_data_fifo[31:0] <= rr_return_data[LW-1:LW-32]; // TODO mux on address bits
               case( arsize_b[1:0] )
                 2'b00:   s_axi_rdata[DW-1:0] <= {4{rr_return_data[7:0]}};  //8-bit
                 2'b01:   s_axi_rdata[DW-1:0] <= {2{rr_return_data[15:0]}}; //16-bit
                 default: s_axi_rdata[DW-1:0] <= rr_return_data[31:0];      //32-bit
                       // s_axi_rdata[DW-1:0] <= rr_return_data_fifo[31:0]; // TODO mux on odd address?
               endcase // case ( arsize_b[1:0] )
             end
          else if( s_axi_rvalid & s_axi_rready )
            rvalid_b <= 1'b0;
       end // else: !if( s_axi_aresetn == 1'b0 )
      else // DW == 64
   always @( posedge s_axi_aclk )
     if (!s_axi_aresetn)
       begin
          rvalid_b        <= 1'b0;
          s_axi_rdata[DW-1:0] <= {(DW){1'b0}};
          s_axi_rresp         <= 2'd0;
          rr_return_data_fifo[LW-1:0] <= {(LW){1'b0}};
       end
     else
       begin
          if( rr_return_access )
            begin
               rvalid_b <= 1'b1;
               s_axi_rresp  <= rr_timeout_access ? 2'b10 : 2'b00;
               case( arsize_b[1:0] )
                 2'b00:   s_axi_rdata[DW-1:0] <= {8{rr_return_data[7:0]}};  //8-bit
                 2'b01:   s_axi_rdata[DW-1:0] <= {4{rr_return_data[15:0]}}; //16-bit
                 2'b10:   s_axi_rdata[DW-1:0] <= {2{rr_return_data[31:0]}}; //32-bit
                 default: s_axi_rdata[DW-1:0] <= rr_return_data[63:0];      //64-bit
               endcase // case ( arsize_b[1:0] )
            end
          else if( s_axi_rvalid & s_axi_rready )
            rvalid_b <= 1'b0;
       end // else: !if( s_axi_aresetn == 1'b0 )
   endgenerate

   //###################################################
   //#TIMEOUT CIRCUIT
   //###################################################  

   reg [1:0] timeout_state;     
`define TIMEOUT_IDLE    2'b00
`define TIMEOUT_ARMED   2'b01
`define TIMEOUT_EXPIRED 2'b10

   always @ (posedge s_axi_aclk)
     if(!s_axi_aresetn)
       timeout_state[1:0] <= `TIMEOUT_IDLE;
     else
       case(timeout_state[1:0])
         `TIMEOUT_IDLE    : timeout_state[1:0] <= (s_axi_arvalid & s_axi_arready ) ? `TIMEOUT_ARMED :
                                                  `TIMEOUT_IDLE;
         `TIMEOUT_ARMED   : timeout_state[1:0] <=  rr_access     ? `TIMEOUT_IDLE    :
                                                   counter_expired ? `TIMEOUT_EXPIRED :
                                                   `TIMEOUT_ARMED;
         `TIMEOUT_EXPIRED : timeout_state[1:0] <= `TIMEOUT_IDLE;
         default : timeout_state[1:0]          <= `TIMEOUT_IDLE;
       endcase // case (timeout_state[1:0])

   //release bus after 64K clock cycles (seems reasonable?)   
   always @ (posedge s_axi_aclk)
     if(timeout_state[1:0]==`TIMEOUT_IDLE)
       timeout_counter[TW-1:0] <= {(TW){1'b1}};   
     else if (timeout_state[1:0]==`TIMEOUT_ARMED)  //decrement while counter > 0
       timeout_counter[TW-1:0] <= timeout_counter[TW-1:0] - 1'b1;

   assign counter_expired     = ~(|timeout_counter[TW-1:0]);
   assign rr_timeout_access = (timeout_state[1:0]==`TIMEOUT_EXPIRED);
        
endmodule // esaxi


