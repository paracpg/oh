module axi_lite_monitor(/*AUTOARG*/
   // Outputs
   trace_16, trace_32,
   // Inputs
   s_axi_awready, s_axi_wready, s_axi_bresp, s_axi_bvalid,
   s_axi_arready, s_axi_rdata, s_axi_rresp, s_axi_rlast, s_axi_rvalid,
   s_axi_awaddr, s_axi_awvalid, s_axi_wdata, s_axi_wstrb,
   s_axi_wvalid, s_axi_bready, s_axi_araddr, s_axi_arprot,
   s_axi_arvalid, s_axi_rready
   );

   parameter          AW                  = 32;
   parameter          DW                  = 32;

   output [15 : 0] trace_16;
   output [31 : 0] trace_32;

   // slave axi outputs
   input          s_axi_awready;
   input          s_axi_wready;
   // input [12-1:0] s_axi_bid; // not on AXI Lite
   input [1 : 0]  s_axi_bresp;
   // input s_axi_buser;  // AXI4
   input          s_axi_bvalid;
   input          s_axi_arready;
   // input [12-1:0] s_axi_rid; // not on AXI Lite
   input [DW-1 : 0] s_axi_rdata;
   input [1 : 0]  s_axi_rresp;
   input          s_axi_rlast;
   // input s_axi_ruser;  // AXI4
   input          s_axi_rvalid;

   // slave axi inputs
   // input [12-1:0]  s_axi_awid; // not on AXI Lite
   input [AW-1 : 0]  s_axi_awaddr;
   // input [7 : 0]   s_axi_awlen; // not on AXI Lite
   // input [2 : 0]   s_axi_awsize; // not on AXI Lite
   // input [1 : 0]   s_axi_awburst; // not on AXI Lite
   // input           s_axi_awlock; // not on AXI Lite
   // input [3 : 0]   s_axi_awcache; // not on AXI Lite
   // input [2 : 0]   s_axi_awprot; // not on AXI Lite
   // input [3 : 0]   s_axi_awregion;  // AXI4
   // input [3 : 0]   s_axi_awqos;  // AXI4
   // input s_axi_awuser;  // AXI4
   input           s_axi_awvalid;
   // input [12-1:0]  s_axi_wid; // not on AXI Lite
   input [DW-1 : 0]  s_axi_wdata;
   input [DW/8-1 : 0]   s_axi_wstrb;
   // input           s_axi_wlast; // not on AXI Lite
   // input s_axi_wuser;  // AXI4
   input           s_axi_wvalid;
   input           s_axi_bready;
   // input [12-1:0]  s_axi_arid; // not on AXI Lite
   input [AW-1 : 0]  s_axi_araddr;
   // input [7 : 0]   s_axi_arlen; // not on AXI Lite
   // input [2 : 0]   s_axi_arsize; // not on AXI Lite
   // input [1 : 0]   s_axi_arburst; // not on AXI Lite
   // input           s_axi_arlock; // not on AXI Lite
   // input [3 : 0]   s_axi_arcache; // not on AXI Lite
   input [2 : 0]   s_axi_arprot;
   // input s_axi_arregion;  // AXI4
   // input [3 : 0]   s_axi_arqos;  // AXI4
   // input s_axi_aruser;  // AXI4
   input           s_axi_arvalid;
   input           s_axi_rready;

   /*AUTOWIRE*/

   //assign trace_16[] = s_axi_awid[12-1:0];
   //assign trace_16[1:0] = s_axi_awlen[1:0];
   //assign trace_16[] = s_axi_awsize[2 : 0];
   assign trace_16[1:0] = {s_axi_wstrb[DW/8-1], s_axi_wstrb[0]};
   //assign trace_16[2] = s_axi_awburst[1];
   //assign trace_16[] = s_axi_awaddr[31:0];
   assign trace_16[2] = s_axi_awvalid;
   assign trace_16[3] = s_axi_awready;
   assign trace_16[4] = s_axi_wvalid;
   assign trace_16[8:5] = s_axi_wdata[3 : 0];
   assign trace_16[9] = s_axi_wready;
   //assign trace_16[] = s_axi_arid[12-1:0];
   //assign trace_16[8:7] = s_axi_arlen[1:0];
   //assign trace_16[] = s_axi_arsize[2 : 0];
   //assign trace_16[] = s_axi_arburst[1 : 0];
   //assign trace_16[] = s_axi_araddr[31:0];
   assign trace_16[10] = s_axi_bvalid;
   assign trace_16[11] = s_axi_bready;
   assign trace_16[12] = s_axi_arvalid;
   assign trace_16[13] = s_axi_arready;
   //assign trace_16[] = s_axi_wid[12-1:0]
   //assign trace_16[] = s_axi_rdata[63 : 0]
   assign trace_16[14] = s_axi_rvalid;
   //assign trace_16[13] = s_axi_rlast;
   //assign trace_16[] = s_axi_rresp[1 : 0];
   assign trace_16[15] = s_axi_rready;
   //assign trace_16[] = s_axi_bid[12-1:0]
   //assign trace_16[] = s_axi_bresp[1 : 0]

   //assign trace_32[] = s_axi_awid[12-1:0];
   //assign trace_32[1:0] = s_axi_awlen[1:0];
   //assign trace_32[4:2] = s_axi_awsize[2 : 0];
   assign trace_32[2:0] = {s_axi_wstrb[DW/8-1], s_axi_wstrb[2], s_axi_wstrb[0]};
   //assign trace_32[7] = s_axi_awburst[1];
   assign trace_32[8:3] = s_axi_awaddr[7:2];
   assign trace_32[9] = s_axi_awvalid;
   assign trace_32[10] = s_axi_awready;
   assign trace_32[11] = s_axi_wvalid;
   assign trace_32[15:12] = s_axi_wdata[3 : 0];
   assign trace_32[16] = s_axi_wready;
   assign trace_32[17] = s_axi_bvalid;
   assign trace_32[18] = s_axi_bready;
   //assign trace_32[] = s_axi_arid[12-1:0];
   //assign trace_32[17:16] = s_axi_arlen[1:0];
   //assign trace_32[20:18] = s_axi_arsize[2 : 0];
   //assign trace_32[] = s_axi_arburst[1];
   assign trace_32[24:19] = s_axi_araddr[7:2];
   assign trace_32[25] = s_axi_arvalid;
   assign trace_32[26] = s_axi_arready;
   //assign trace_32[] = s_axi_wid[12-1:0]
   assign trace_32[29:27] = s_axi_rdata[2:0];
   assign trace_32[30] = s_axi_rvalid;
   //assign trace_32[29] = s_axi_rlast;
   //assign trace_32[] = s_axi_rresp[1 : 0];
   assign trace_32[31] = s_axi_rready;
   //assign trace_32[] = s_axi_bid[12-1:0]
   //assign trace_32[] = s_axi_bresp[1 : 0]

endmodule // axi_monitor
