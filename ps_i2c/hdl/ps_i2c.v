
module ps_i2c (/*AUTOARG*/
   // Outputs
   i2c_sda_i, i2c_scl_i, constant_zero, constant_one,
   // Inouts
   i2c_sda, i2c_scl,
   // Inputs
   i2c_sda_t, i2c_sda_o, i2c_scl_t, i2c_scl_o
   );

   // constants
   output		constant_zero;		// Always 0
   output		constant_one;		// Always 1

   /*AUTOINPUT*/
   // Beginning of automatic inputs (from unused autoinst inputs)
   input                i2c_scl_o;              // To pi2c_inst of pi2c.v
   input                i2c_scl_t;              // To pi2c_inst of pi2c.v
   input                i2c_sda_o;              // To pi2c_inst of pi2c.v
   input                i2c_sda_t;              // To pi2c_inst of pi2c.v
   // End of automatics

   /*AUTOINOUT*/
   // Beginning of automatic inouts (from unused autoinst inouts)
   inout                i2c_scl;                // To/From pi2c_inst of pi2c.v
   inout                i2c_sda;                // To/From pi2c_inst of pi2c.v
   // End of automatics

   /*AUTOOUTPUT*/
   // Beginning of automatic outputs (from unused autoinst outputs)
   output               i2c_scl_i;              // From pi2c_inst of pi2c.v
   output               i2c_sda_i;              // From pi2c_inst of pi2c.v
   // End of automatics

   assign constant_zero = 1'b0;
   assign constant_one = 1'b1;

   //################################
   //# call GPIO module
   //################################
   pi2c pi2c_inst
     (
      /*AUTOINST*/
      // Outputs
      .i2c_sda_i                        (i2c_sda_i),
      .i2c_scl_i                        (i2c_scl_i),
      // Inouts
      .i2c_sda                          (i2c_sda),
      .i2c_scl                          (i2c_scl),
      // Inputs
      .i2c_sda_o                        (i2c_sda_o),
      .i2c_sda_t                        (i2c_sda_t),
      .i2c_scl_o                        (i2c_scl_o),
      .i2c_scl_t                        (i2c_scl_t));

endmodule // ps_pi2c

// Local Variables:
// verilog-library-directories:("." "../../*/hdl")
// End:
