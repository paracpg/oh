# NOTE: See UG1118 for more information

set design ps_i2c
set projdir ./
set root "../.."

set hdl_files [list \
                   $root/ps_i2c/hdl \
                   $root/parallella/hdl \
                  ]

set ip_files   []

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
