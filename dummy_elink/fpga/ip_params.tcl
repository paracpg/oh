# NOTE: See UG1118 for more information

set design dummy_elink
set projdir ./
set root "../.."

set hdl_files [list \
	           $root/dummy_elink/hdl \
		  ]

set ip_repos [list \
                  $root/parallella/ip_repo \
                 ]

set ip_files   []

set constraints_files []

source $root/parallella/fpga/synthesis_common.tcl
